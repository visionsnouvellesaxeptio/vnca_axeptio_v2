<?php
/**
 * Plugin Name: VN Axeptio V2 (RGPD)
 * Plugin URI: https://www.visionsnouvelles.com
 * Description: Plugin développé par Visions Nouvelles permettant de lier Axeptio au site.
 * Version: 2.0.0
 * Author: Visions Nouvelles
 * Author URI: https://www.visionsnouvelles.com
 * License: Copyright ©2018 Visions Nouvelles pour une utilisation exclusive des clients de Visions Nouvelles
 */

namespace vnca_axeptio;

use vnca_axeptio\App\App;
use vnca_axeptio\App\Api\Api;

defined('ABSPATH') or die('No script kiddies please!');

define('VNCA_TEXTDOMAIN', 'vnca_axeptio');
define('VNCA_FOLDER', basename(dirname(__FILE__)) . DIRECTORY_SEPARATOR);
define('VNCA_DIR', plugin_dir_path(__FILE__));
define('VNCA_DIR_VIEW', plugin_dir_path(__FILE__) . 'views');
define('VNCA_URL', plugin_dir_url(__FILE__));
define('VNCA_SCRIPT_NEEDED', VNCA_URL . 'assets/script_needed/');
define('PLUGIN_VERSION', '2.1.0');

define('PLUGIN_API_VERSION', '1');
define('PLUGIN_API_PATH', VNCA_TEXTDOMAIN . '/v' . PLUGIN_API_VERSION);

require_once(dirname(__FILE__) . '/composer/vendor/autoload.php');

class vnca_axeptio
{
    private static $app;
    private static $api;

    public function __construct()
    {
        self::$app = App::getInstance();
        self::$api = Api::getInstance();
        register_activation_hook(__FILE__, array(vnca_axeptio::class, 'activation'));
        register_deactivation_hook(__FILE__, array(vnca_axeptio::class, 'deactivation'));

        // initialize the api of the plugin
        add_action( 'rest_api_init', array($this, 'startApiRest'));
    }

    public function execute()
    {
        self::$app->execute();
    }

    public function startApiRest(){
        self::$api->execute();
    }

    public static function activation()
    {
        self::$app->activation();
    }

    public static function deactivation()
    {
        self::$app->deactivation();
    }
}

$vn_axeptio = new vnca_axeptio();
$vn_axeptio->execute();