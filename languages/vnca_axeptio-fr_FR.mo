��    1      �  C   ,      8     9  E   F     �     �     �     �  %   �  6        9     E     S     \     c     r     �     �  !   �     �     �     �  
   �  ;        C     R     b  
   q     |  2   �  1   �     �               9  /   G     w     �  
   �  	   �     �  "   �  V        X     g     s     �     �  G   �     �  E  �     <  V   J     �     �     �     �  /   �  .   )     X  	   d     n     |     �     �  -   �     �  ,   �          *     9  
   N  J   Y     �     �     �  !   �     �  4     3   C  2   w     �  #   �     �  H   �  $   ;     `  
   }  
   �  &   �  0   �  g   �     S     q     �     �     �  G   �     �     -         (       '                              "   #      .   *   )                     &      	            %                1                !                     
               +   0   $       ,   /                                  API KEY (V3) Admin: No widget found for newsletter please check in the back office Alertify.js (Script) Alertify.js (Style) Authentification failed Bad parameters Click on the link to get your API key Connection to the newsletter system has been completed Description Directory of  Download E-mail E-mail Axeptio Email address Export only valid consent Export the consents Form validation bootstrap (Style) GDPR consents GDPR settings Global settings ID Axeptio Informs the user if he has not correctly completed the form Manage scripts Message Axeptio NAME_OF_WIDGET Newsletter Newsletter system Notified the user according to the steps (Script). Notified the user according to the steps (Style). Overload CDN are running : Password Axeptio Please give your consent. RGPD settings Remove consent in Axeptio when user unsubscribe Select newsletter system Select newsletter widget SendinBlue ShortCode The newsletter system not exist The user has correctly unsubscribe Unsubscription to the newsletter could not be applied, please contact the site manager Use custom CDN Use the CDN VNCA form newsletter Widget Widget name Widgets available on your Axeptio account that can be used on your site You need scripts ? Project-Id-Version: Plugin vn export consentements axeptio
PO-Revision-Date: 2020-11-05 12:05+0100
Last-Translator: 
Language-Team: Visions Nouvelles <webmaster@visionsnouvelles.com>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-KeywordsList: _;__;_e;_n:1,2;_x;gettext;gettext_noop;ngettext;esc_attr__;esc_attr_e;esc_html__;esc_html_e
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=n == 1 ? 0 : 1;
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: ..
 Clé API (V3) Admin: Aucun widget trouvé pour la newsletter, veuillez vérifier dans le back-office Alertify.js (Script) Alertify.js (Style) Authentification échouée Mauvais paramètres Cliquez sur le lien pour obtenir votre clé API Connexion au système de newsletter complété Description Liste de  Télécharger E-mail E-mail Axeptio Adresse e-mail Exporter uniquement les consentements valides Exporter les consentements Validation de formulaire - Bootstrap (Style) RGPD consentements Réglages RGPD Réglages généraux ID Axeptio Informe l’utilisateur s’il n’a pas correctement rempli le formulaire Gérer les scripts Message Axeptio NOM_DU_WIDGET Lettre d'information (newsletter) Système de newsletter Notifié l’utilisateur selon les étapes (Script). Notifié l’utilisateur selon les étapes (Style). Les CDN de surcharge sont en cours d’exécution: Mot de passe Axeptio Veuillez donner votre consentement. Réglages RGPD Supprimer le consentement dans Axeptio quand l'utilisateur se désincrit Sélection du système de newsletter Sélection newsletter widget SendinBlue Code court Le système de newsletter n'existe pas L’utilisateur s’est correctement désabonné La désinscription de la newsletter n'a pu être appliqué, veuillez contacter l'administrateur du site Utiliser un CDN personnalisé Utilisez le CDN VNCA Formulaire de newsletter Widget Nom du widget Widgets disponibles sur votre compte Axeptio utilisables sur votre site Vous avez besoin de scripts ? 