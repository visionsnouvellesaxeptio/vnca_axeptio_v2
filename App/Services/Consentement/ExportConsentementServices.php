<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 12/02/2020
 * Time: 09:18
 */

namespace vnca_axeptio\App\Services\Consentement;

use Exception;
use vnca_axeptio\App\Component\Axeptio\Axeptio;
use vnca_axeptio\App\Repository\ConsentementRepository;
use vnca_axeptio\Core\Singleton;

class ExportConsentementServices
{
    use Singleton;

    public $file;
    public $fileStream;
    public static $delimiter = ';';
    private $processings = [];

    public function __instance()
    {
        $this->file = tempnam(sys_get_temp_dir(), wp_rand());
        $this->fileStream = fopen($this->file, 'w');
        $this->processings = Axeptio::getInstance()->getAllProcessing();
    }


    /**
     *
     * Function to create and read in CSV file the data of the database
     * @param null $email
     * @param null $widget_id
     * @param int $only_valid
     * @return string
     * @throws Exception
     */
    public function getExportCsv($email = null, $widget_id = null, $only_valid = 0)
    {

        $this->addElement('Email', 'Token', 'Widget id', 'Widget name', 'Timestamp', 'Accepted');

        if (!empty($email)) {
            $this->initBodyCsvByEmailUser($email, $widget_id, $only_valid);
        } elseif (!empty($widget_id) && $only_valid){
            $this->initBodyCsvByValidateConsentAndWidget($widget_id, $only_valid);
        }

        $base = base64_encode(file_get_contents($this->file));
        fclose($this->fileStream);
        return $base;
    }

    public function addElement($email, $token, $widget_id, $widget_name, $created_at, $accepted)
    {
        fputcsv($this->fileStream, array($email, $token, $widget_id, $widget_name, $created_at, $accepted), self::$delimiter);
    }


    /**
     * @param $email
     * @param null $widget_id
     * @param bool $only_valid
     * @return null
     * @throws Exception
     */
    private function initBodyCsvByEmailUser($email, $widget_id = null, $only_valid = false)
    {
        $nb_element = 0;
        $user = ConsentementRepository::getInstance()->getUserByEmail($email);
        if (empty($user))
            return null;

        $userInformation = Axeptio::getInstance()->getUserConsentInformation($user);

        if ($only_valid || !empty($widget_id)) {
            $validConsent = [];

            // init the $validConsent withe require widget_id
            foreach ($userInformation->state as $key => $is_validate) {
                if (
                    ($is_validate && $only_valid) && (empty($widget_id)) ||  // only consent validate
                    (!empty($widget_id) && $key == $widget_id) && $only_valid == false || // only widget
                    ($is_validate && $only_valid) && (!empty($widget_id) && $key == $widget_id) // widget_id and consent validate
                ) {
                    $validConsent[$key] = $is_validate;
                }
            }

            // Add element if is the first
            $index = 0;
            while (!empty($validConsent) || sizeof($userInformation->history) >= $index) {
                $element = (!empty($userInformation->history[$index])) ? $userInformation->history[$index] : [];

                if (!empty($element) && in_array($element->identifier, $validConsent)) {
                    $nb_element ++;
                    $this->addElement($user->getEmail(), $user->getToken(), $element->identifier, $this->processings[$element->identifier], $element->timestamp, ($element->accept) ? '1' : '0');
                    unset($validConsent[$userInformation->history[$index]->identifier]);
                }
                $index++;
            }

        } else {
            // Get all consent of user
            foreach ($userInformation->history as $history) {
                $nb_element ++;
                $this->addElement($user->getEmail(), $user->getToken(), $history->identifier, $this->processings[$history->identifier], $history->timestamp, ($history->accept) ? '1' : '0');
            }
        }

        if ($nb_element == 0){
            throw new Exception(__('No result for this user', VNCA_TEXTDOMAIN));
        }

        return null;
    }

    private function initBodyCsvByValidateConsentAndWidget($widget_id, $only_valid)
    {
        $information = Axeptio::getInstance()->getValidateConsentByWidget($widget_id);
        if (empty($information))
            throw new Exception(__('No consent for this box', VNCA_TEXTDOMAIN));

        foreach ($information->tokens as $token) {
            $user = ConsentementRepository::getInstance()->getUserByToken($token->token);
            if (empty($user))
                continue;

            $this->addElement($user->getEmail(), $user->getToken(), $widget_id, $this->processings[$widget_id], $token->acceptedAt, '1');
        }
    }

}