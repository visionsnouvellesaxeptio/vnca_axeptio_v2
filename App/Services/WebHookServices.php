<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 13/02/2020
 * Time: 12:00
 */

namespace vnca_axeptio\App\Services;
use vnca_axeptio\App\Component\Axeptio\Axeptio;
use vnca_axeptio\App\Component\Axeptio\Connection;
use vnca_axeptio\Core\Singleton;

class WebHookServices
{
    use Singleton;
}