<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 21/11/2019
 * Time: 12:22
 */

namespace vnca_axeptio\App\Entity;


use vnca_axeptio\Core\Entity\T_EntityRepository;
use vnca_axeptio\App\Repository\NewsletterUnsubscribeRepository;

class NewsletterUnsubscribeEntity
{
    use T_EntityRepository;

    /**
     * @var NewsletterUnsubscribeRepository
     */
    static $repository = NewsletterUnsubscribeRepository::class;

    private $id = null;
    private $identifier_axeptio = null;
    private $token_user = null;
    private $source = null;
    private $date = null;

    public function __construct($array_object = array())
    {
        $this->id = $this->getArrayAttribute($array_object, 'id', null);
        $this->identifier_axeptio = $this->getArrayAttribute($array_object, 'identifier_axeptio', null);
        $this->token_user = $this->getArrayAttribute($array_object, 'token_user', null);
        $this->source = $this->getArrayAttribute($array_object, 'source', null);
        $this->date = $this->getArrayAttribute($array_object, 'date', null);
    }

    /**
     * @param string $repository
     */
    public static function setRepository($repository)
    {
        self::$repository = $repository;
    }

    /**
     * @return null|string
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getIdentifierAxeptio()
    {
        return $this->identifier_axeptio;
    }

    public function setIdentifierAxeptio($identifier_axeptio)
    {
        $this->identifier_axeptio = $identifier_axeptio;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTokenUser()
    {
        return $this->token_user;
    }

    public function setTokenUser($token_user)
    {
        $this->token_user = $token_user;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSource()
    {
        return $this->source;
    }


    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDate()
    {
        return $this->date;
    }


    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

}