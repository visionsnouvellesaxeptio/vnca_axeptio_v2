<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 21/11/2019
 * Time: 12:22
 */

namespace vnca_axeptio\App\Entity;

use vnca_axeptio\Core\Entity\T_EntityRepository;
use vnca_axeptio\App\Repository\ConsentementRepository;

class ConsentementEntity
{
    use T_EntityRepository;

    /**
     * @var ConsentementRepository
     */
    static $repository = ConsentementRepository::class;

    private $id = null;
    private $email = null;

    private $token = null;

    public function __construct($array_object = array())
    {
        $this->id = $this->getArrayAttribute($array_object, 'id_consentement', null);
        $this->email = $this->getArrayAttribute($array_object, 'email', null);
        $this->token = $this->getArrayAttribute($array_object, 'token', null);
    }

    /**
     * @return null|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return null|string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Not used caused is in autoincrement
     *
     * @param null|string $id
     * @return ConsentementEntity
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param null|string $email
     * @return ConsentementEntity
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param null|string $token
     * @return ConsentementEntity
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

}