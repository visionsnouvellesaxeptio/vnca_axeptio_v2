<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 21/11/2019
 * Time: 12:22
 */

namespace vnca_axeptio\App\Entity;


use vnca_axeptio\Core\Entity\T_EntityRepository;
use vnca_axeptio\App\Repository\WebHookRepository;

class WebHookEntity
{
    use T_EntityRepository;

    static $repository = WebHookRepository::class;

    private $id_newsletter = null;
    private $description = null;
    private $newsletter_name = null;

    public function __construct($array_object = array())
    {
        $this->id_newsletter = $this->getArrayAttribute($array_object, 'id_newsletter', null);
        $this->description = $this->getArrayAttribute($array_object, 'description', null);
        $this->newsletter_name = $this->getArrayAttribute($array_object, 'newsletter_name', null);
    }

    /**
     * @return null|string
     */
    public function getIdNewsletter()
    {
        return $this->id_newsletter;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return null|string
     */
    public function getNewslettername()
    {
        return $this->newsletter_name;
    }

    /**
     * @param null|string $id_newsletter
     * @return WebHookEntity
     */
    public function setIdNewsletter($id_newsletter)
    {
        $this->id_newsletter = $id_newsletter;
        return $this;
    }

    /**
     * @param null|string $description
     * @return WebHookEntity
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @param null|string $newsletter_name
     * @return WebHookEntity
     */
    public function setNewslettername($newsletter_name)
    {
        $this->newsletter_name = $newsletter_name;
        return $this;
    }

}