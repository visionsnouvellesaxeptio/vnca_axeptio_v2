<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 18/10/2019
 * Time: 12:10
 */

namespace vnca_axeptio\App\Component;


use vnca_axeptio\App\Component\Shortcode\BoxAxeptioShortcode;
use vnca_axeptio\App\Component\Shortcode\FormNewsletterShortcode;
use vnca_axeptio\Core\Singleton;

class Shortcode
{
    use Singleton;

    protected function __instance()
    {
        add_action('wp_enqueue_scripts', array($this, 'wp_enqueue_scripts'));
        add_action( 'init', [$this, 'init'] );
    }

    public function init(){
        BoxAxeptioShortcode::getInstance();
        FormNewsletterShortcode::getInstance();
    }

    public function wp_enqueue_scripts(){}
}