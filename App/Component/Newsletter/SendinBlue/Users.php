<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 22/11/2019
 * Time: 15:22
 */

namespace vnca_axeptio\App\Component\Newsletter\SendinBlue;


use vnca_axeptio\Core\Response;
use vnca_axeptio\Core\Singleton;
use vnca_axeptio\App\Entity\ConsentementEntity;

class Users
{
    use Singleton;

    public function createUser(ConsentementEntity $user_data)
    {

        $user = $this->getUser($user_data->getEmail());


        if (!$user) {
            $response = Api::call("contacts/", [
                "email" => $user_data->getEmail(),
                "listIds" => [(int)get_option('vnca_sendinblue_list_id')]
            ], 'POST');

            //file_put_contents(VNCA_DIR . 'logs/axeptiov2.log', '[' . date('d/m/Y - H:i:s') . '] ' . print_r('if not user', true) . PHP_EOL, FILE_APPEND | LOCK_EX);
            //file_put_contents(VNCA_DIR . 'logs/axeptiov2.log', '[' . date('d/m/Y - H:i:s') . '] ' . print_r($response, true) . PHP_EOL, FILE_APPEND | LOCK_EX);
            $user_id = ($response->getCode() == 201) ? $response->getBody()->id : false;
        } else {
            $user_id = $user->id;

            $response = Api::call("contacts/". $user_data->getEmail(), [
                "listIds" => [(int)get_option('vnca_sendinblue_list_id')]
            ], 'PUT');

            //file_put_contents(VNCA_DIR . 'logs/axeptiov2.log', '[' . date('d/m/Y - H:i:s') . '] ' . print_r('if user', true) . PHP_EOL, FILE_APPEND | LOCK_EX);
            //file_put_contents(VNCA_DIR . 'logs/axeptiov2.log', '[' . date('d/m/Y - H:i:s') . '] ' . print_r($response, true) . PHP_EOL, FILE_APPEND | LOCK_EX);

            $user_id = ($response->getCode() == 204) ? $user_id : false;
        }

        return $user_id;
    }

    public function deleteUser(ConsentementEntity $user_data)
    {
        $response = Api::call("contacts/". $user_data->getEmail(), [
            "unlinkListIds" => [(int)get_option('vnca_sendinblue_list_id')]
        ], 'PUT');

        return ($response->getCode() == 204) ? true : false;
    }

    /**
     * @param $email
     * @return int
     */
    private function userExist($email)
    {
        $response = Api::call("contacts/".$email, [], 'GET');

        return ($response->getCode() == 200);
    }

    /**
     * @param $email
     * @return int|Response
     */
    private function getUser($email)
    {
        $response = Api::call("contacts/".$email, [], 'GET');

        return ($response->getCode() == 200) ? $response->getBody() : false;
    }

}