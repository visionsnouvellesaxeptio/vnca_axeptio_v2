<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 20/11/2019
 * Time: 16:50
 */

namespace vnca_axeptio\App\Component\Newsletter\SendinBlue;


use vnca_axeptio\Core\Newsletter\Fact_Newsletter;
use vnca_axeptio\App\Entity\WebHookEntity;
use vnca_axeptio\App\Repository\WebHookRepository;

class WebHook
{
    private static $instance = null;
    private static $webHookRepository = null;

    private function __construct()
    {
        self::$webHookRepository = WebHookRepository::getInstance();
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new WebHook();
        }
        return self::$instance;
    }

    public function generateWebHook($url, $description, $events, $type = Api::TYPE_MARKETING)
    {
        $this->deleteAllWebHook();
        $name_newsletter = Fact_Newsletter::getCurrentNameNewsletter();

        $events = (is_array($events)) ? $events : array($events);
        $response = Api::call("/webhooks", [
            'url' => $url,
            'description' => $description,
            'events' => $events,
            'type' => $type
        ]);

        $response_code = $response->getCode();
        $response = $response->getBody();
        if ($response_code == 201) {
            $webhookentity = new WebHookEntity();
            $webhookentity->setIdNewsletter($response->id)
                ->setDescription($description)
                ->setNewslettername($name_newsletter);

            WebHookRepository::getInstance()->addWebHook($webhookentity);
        }
    }

    public function deleteWebHook(WebHookEntity $entity)
    {
        $response = Api::call('webhooks/'. $entity->getIdNewsletter(), [], 'DELETE');
        $response_code = $response->getCode();

        if ($response_code == 204) {
            WebHookRepository::getInstance()->deleteWebHook($entity);
        }
    }

    public function deleteAllWebHook()
    {
        $repository = WebHookRepository::getInstance();
        $entities = $repository->getHookByNewsletter(SendinBlue::getName());
        foreach ($entities as $entity){
            $this->deleteWebHook($entity);
        }
    }
}