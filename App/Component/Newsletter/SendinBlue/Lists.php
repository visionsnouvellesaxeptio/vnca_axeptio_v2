<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 22/11/2019
 * Time: 15:22
 */

namespace vnca_axeptio\App\Component\Newsletter\SendinBlue;


use vnca_axeptio\Core\Singleton;

class Lists
{
    use Singleton;

    public function createList()
    {
        $list_id = get_option('vnca_sendinblue_list_id', -1);

        if ($this->listExist($list_id)) {
            return $list_id;
        }

        $directory_id = $this->createDirectory();
        if ($directory_id != null && $directory_id != '') {
            $response = Api::call("contacts/lists", [
                "name" => "RGPD Axeptio : ".get_bloginfo('name'),
                "folderId" => (int)$directory_id
            ], 'POST');

            $response_code = $response->getCode();
            $response = $response->getBody();


            if ($response_code == 201) {
                $list_id = $response->id;
                update_option('vnca_sendinblue_list_id', $list_id);
            } else {
                error_log("the folder of sendinBlue is not created");
            }
        }

        return $list_id;
    }

    public function createDirectory()
    {
        $directory_id = get_option('vnca_sendinblue_directory_id', -1);

        if ($this->directoryExist($directory_id)) {
            return $directory_id;
        }

        $response = Api::call("contacts/folders", [
            "name" => __('Directory of ', VNCA_TEXTDOMAIN) . apply_filters('the_title', get_bloginfo('name'))
        ], 'POST');

        $response_code = $response->getCode();
        $response = $response->getBody();
        if ($response_code == 201) {
            $directory_id = $response->id;
            update_option('vnca_sendinblue_directory_id', $directory_id);
        } else {
            return null;
            error_log("the folder of sendinBlue is not created");
        }

        return $directory_id;
    }

    function directoryExist($folder_id)
    {
        $response = Api::call("contacts/folders/" . $folder_id, [], 'GET');

        $response_code = $response->getCode();

        return ($response_code == 200);
    }

    function listExist($list_id)
    {
        $response = Api::call("contacts/lists/" . $list_id, [], 'GET');
        $response_code = $response->getCode();

        return ($response_code == 200);
    }

    function deleteList($list_id)
    {
        $response = Api::call("contacts/lists/" . $list_id, [], 'DELETE');
        $response_code = $response->getCode();

        if ($response_code == 204) {
            update_option('vnca_sendinblue_list_id', null);
            return true;
        } else {
            return false;
        }
    }
}