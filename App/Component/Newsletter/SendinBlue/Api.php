<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 20/11/2019
 * Time: 17:14
 */

namespace vnca_axeptio\App\Component\Newsletter\SendinBlue;



use vnca_axeptio\Core\Response;

class Api
{
    const BASE_URL = 'https://api.sendinblue.com/v3/';

    const EVENT_HARD_BOUNCE = 'hardBounce';
    const EVENT_SOFT_BOUNCE = 'softBounce';
    const EVENT_BLOCKED = 'blocked';
    const EVENT_SPAM = 'spam';
    const EVENT_DELIVERED = 'delivered';
    const EVENT_REQUEST = 'request';
    const EVENT_CLICK = 'click';
    const EVENT_INVALID = 'invalid';
    const EVENT_DEFERRED = 'deferred';
    const EVENT_OPENED = 'opened';
    const EVENT_UNIQ_OPENED = 'uniqueOpened';
    const EVENT_UNSUBSCRIBED = 'unsubscribed';
    const EVENT_LIST_ADDITION = 'listAddition';
    const EVENT_CONTRACT_UPDATE = 'contactUpdated';
    const EVENT_CONTRACT_DELETED = 'contactDeleted';

    const TYPE_MARKETING = 'marketing';
    const TYPE_TRANSACTIONAL = 'transactional';

    public static function isValidateConnexion($request = [])
    {
        if (!isset($request['vnca_sendinblue_token'])) {
            return false;
        }
        $option_old = get_option('vnca_sendinblue_token');
        update_option('vnca_sendinblue_token', $request['vnca_sendinblue_token']);
        $response = self::call('/account', array(), 'GET');
        update_option('vnca_sendinblue_token', $option_old);

        return ($response->getCode() == 200);
    }

    /**
     * @param $endpoint
     * @param array $args
     * @param string $method
     * @return Response
     */
    public static function call($endpoint, $args = array(), $method = 'POST')
    {
        $http = new \WP_Http();

        $api_key = get_option('vnca_sendinblue_token');
        $api_key = (!is_wp_error($api_key) && $api_key != '') ? $api_key : '';

        $request = [
            'method' => $method,
            'headers' => [
                'Content-Type' => 'application/json',
                'api-key' => $api_key
            ]
        ];

        if (! empty($args)){
            $request['body'] = json_encode($args);
        }

        $response = $http->request(self::BASE_URL . $endpoint, $request);

        return new Response($response);
    }
}



