<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 18/11/2019
 * Time: 17:24
 */

namespace vnca_axeptio\App\Component\Newsletter\SendinBlue;

use vnca_axeptio\App\Api\Controller\Controller;
use vnca_axeptio\App\Component\Axeptio\AxeptioNewsletter;
use vnca_axeptio\Core\Newsletter\I_Newsletter;
use vnca_axeptio\Core\Singleton;
use vnca_axeptio\App\Entity\ConsentementEntity;
use vnca_axeptio\App\Repository\ConsentementRepository;

class SendinBlue implements I_Newsletter
{
    use Singleton;

// region -----------------------------------  Initialization ---------------------------------

    public function isValideConnexion($request = [])
    {
        return Api::isValidateConnexion($request);
    }

    public function initialized()
    {
        $this->createList();

        // delete and create webhooks
        WebHook::getInstance()->deleteAllWebHook();
        WebHook::getInstance()->generateWebHook(
            Controller::getRoute("webhook_newsletter_unsubscribe_response"),
            __('Remove consent in Axeptio when user unsubscribe', VNCA_TEXTDOMAIN),
            Api::EVENT_UNSUBSCRIBED
        );

        return true;
    }

    public function uninitialized() {
        WebHook::getInstance()->deleteAllWebHook();
        Lists::getInstance()->deleteList(get_option('vnca_sendinblue_list_id'));
    }

// endregion

// region ----------------------------------------  User --------------------------------------

    public function createUser(ConsentementEntity $user_data)
    {
        //file_put_contents(VNCA_DIR . 'logs/axeptiov2.log', '[' . date('d/m/Y - H:i:s') . '] ' . print_r($user_data, true) . PHP_EOL, FILE_APPEND | LOCK_EX);

        Users::getInstance()->createUser($user_data);
    }

    public function deleteUser(ConsentementEntity $user_data)
    {
        return Users::getInstance()->deleteUser($user_data);
    }

    public function getUser($user_data)
    {
        // TODO: Implement getUser() method.
    }

// endregion

// region ----------------------------------------  List --------------------------------------

    public function createList()
    {
        return Lists::getInstance()->createList();
    }

    public static function getName()
    {
        return 'SendinBlue';
    }

    public function userUnsubscribe($request)
    {
        if (isset($request['email']) && isset($request['event']) && $request['event'] == 'unsubscribe'){
            $return =  AxeptioNewsletter::getInstance()->useWasUnsubscribe($request['email'], self::getName());
            if($return){
                return self::deleteUser(ConsentementRepository::getInstance()->getUserByEmail($request['email']));
            }
        }

        return false;
    }

}
