<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 18/10/2019
 * Time: 12:10
 */

namespace vnca_axeptio\App\Component;


use vnca_axeptio\Core\Singleton;
use vnca_axeptio\App\Component\Widget\component\FormNewsletterWidgetComponent;

class Widget
{

    use Singleton;

    protected function __instance()
    {
        add_action('wp_enqueue_scripts', array($this, 'wp_enqueue_scripts'));
    }

    public function init(){
        FormNewsletterWidgetComponent::getInstance();
    }

    public function wp_enqueue_scripts(){}
}