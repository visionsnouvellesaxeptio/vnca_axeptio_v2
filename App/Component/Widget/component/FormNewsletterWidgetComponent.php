<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 30/04/2020
 * Time: 15:15
 */

namespace vnca_axeptio\App\Component\Widget\component;

use vnca_axeptio\Core\Singleton;

class FormNewsletterWidgetComponent extends \WP_Widget
{
    use Singleton;

    private static $widget_id = 'FormNewsletterWidgetComponent';

    function __construct()
    {
        parent::__construct(
            self::$widget_id,
            __('VNCA form newsletter', VNCA_TEXTDOMAIN)
        );
        add_action('widgets_init', [$this, 'init']);
    }

    public function init()
    {
        register_widget($this);
    }

    // Creating widget front-end
    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        echo do_shortcode('[vnca_form_newsletter]');
        echo $args['after_widget'];
    }

    // Widget Backend
    public function form($instance)
    {}

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        return $instance;
    }
}