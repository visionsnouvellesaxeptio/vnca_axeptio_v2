<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 23/10/2019
 * Time: 14:21
 */

namespace vnca_axeptio\App\Component\Axeptio;


class SDK
{
    private static $instance_sdk = null;
    private static $is_enqueue = false;
    private static $is_register = false;
    static $name = 'SDK_axeptio';

    private function __construct()
    {}

    public function loadSDK()
    {
        if (!self::$is_enqueue) {
            wp_localize_script(self::$name, 'option', ['id_project' => get_option('vnca_axeptio_id'), 'cookie_version' => get_option('vnca_cookie_version')]);
            wp_enqueue_script(self::$name);
            self::$is_enqueue = true;
        }
    }

    public function register()
    {
        if (!self::$is_register) {
            wp_register_script(self::$name, VNCA_URL . 'assets/js/axeptio/load_sdk.js', 'jquery', PLUGIN_VERSION, true);
            self::$is_register = true;
        }
        return self::$name;
    }

    /**
     * @return mixed
     */
    public static function getInstance()
    {
        if (self::$instance_sdk == null) {
            self::$instance_sdk = new SDK();
        }
        return self::$instance_sdk;
    }
}