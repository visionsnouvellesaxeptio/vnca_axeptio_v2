<?php


namespace vnca_axeptio\App\Component\Axeptio;


use vnca_axeptio\App\Api\Controller\Controller;
use vnca_axeptio\App\Entity\ConsentementEntity;
use vnca_axeptio\Core\Singleton;

class Consents
{
    use Singleton;

    /**
     * @var Connection|null
     */
    private $connection;

    public function __instance()
    {
        $this->connection = Connection::getInstance();
    }

    /**
     * @param $user ConsentementEntity
     * @return array|mixed
     */
    public function getUserConsentInformation($user){
        $current_project = Axeptio::$current_project;
        $user_token = $user->getToken();
        $information = $this->connection->call(
            "client/{$current_project}/consents/{$user_token}",
            [],
            'GET'
        );

        return (isset($information)) ? $information : [];
    }


    public function getValidateConsentByWidget($widget_id){
        $current_project = Axeptio::$current_project;
        $information = $this->connection->call(
            "client/{$current_project}/tokens/accepted/{$widget_id}",
            [],
            'GET'
        );

        return (isset($information)) ? $information : [];
    }
}