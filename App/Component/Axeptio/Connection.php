<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 19/11/2019
 * Time: 15:46
 */

namespace vnca_axeptio\App\Component\Axeptio;

use vnca_axeptio\App\PageSetting\GeneralOption;

class Connection
{
    const BASE_URL = 'https://api.axept.io/v1/';
    const TIME_VALIDATE_TOKEN = 'P15D';

    public static $instance = null;
    private static $date_token_validate = null;

    private static $email = null;
    private static $password = null;
    private static $token = null;

    private function __construct()
    {

        self::$date_token_validate = new \DateTime();
        self::$date_token_validate->add(new \DateInterval(self::TIME_VALIDATE_TOKEN));

        self::$email = get_option('vnca_axeptio_email');
        self::$password = GeneralOption::getInstance()->getAxeptioPassword();
        self::$token = $this->generateToken();

    }

    static function getInstance()
    {
        if (self::$instance == null || new \DateTime() > self::$date_token_validate) {
            self::$instance = new Connection();
        }

        return self::$instance;
    }

    public function generateToken()
    {

        $response = $this->call('auth/local/signin', array(
            "username" => self::getEmail(),
            "password" => self::getPassword(),
        ), 'POST');


        return (!isset($response->token)) ? null : $response->token;

    }

    public function call($endpoint, $args = array(), $method = 'POST', $headers = [])
    {
        $curl = curl_init();

        $array_header = [
            "cache-control: no-cache",
            "content-type: application/json",
        ];

        if (self::$token !== null) {
            array_push($array_header, 'Authorization: Bearer ' . self::$token);
        }

        foreach ($headers as $key => $value) {
            array_push($array_header, $key . ': ' . $value);
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::BASE_URL . $endpoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => json_encode($args),
            CURLOPT_HTTPHEADER => $array_header,
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return json_decode($response);
    }

    private static function getToken()
    {
        return self::$token;
    }

    private static function getEmail()
    {
        return self::$email;
    }

    private static function getPassword()
    {
        return self::$password;
    }

    public function __toString()
    {
        return "" . self::getToken();
    }
}