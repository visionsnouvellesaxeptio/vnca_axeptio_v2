<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 19/11/2019
 * Time: 15:42
 */

namespace vnca_axeptio\App\Component\Axeptio;

use vnca_axeptio\App\Api\Controller\Controller;
use vnca_axeptio\App\Entity\ConsentementEntity;
use vnca_axeptio\Core\Singleton;

class Axeptio
{
    use Singleton;

    /**
     * @var Connection
     */
    private static $api_connection = false;
    public static $processing = [];
    public static $current_project;
    public static $webhook_id;

    private function __instance()
    {
        self::$api_connection = Connection::getInstance();
        self::$current_project = get_option('vnca_axeptio_id');
        self::$webhook_id = get_option('vnca_axeptio_webhook_id');
    }

    public function getNameOfProcessing($processing)
    {
        $processing = self::$api_connection->call('vault/processings/' . $processing, array(), 'GET');
        return (!isset($processing->data->name)) ? null : $processing->data->name;
    }

    public function getAllProcessing()
    {
        if(empty(self::$processing)){
            $processings = self::$api_connection->call('vault/processings/', array(), 'GET');
            $array_processing = [];

            foreach ($processings as $processing) {
                if (isset($processing->data->projectId) && $processing->data->projectId == self::$current_project) {
                    $array_processing[$processing->id] = $processing->data->name;
                }
            }
            self::$processing = $array_processing;
        }

        return self::$processing;
    }

    public function getProcessingById($processing_id)
    {
        $processing = self::$api_connection->call('vault/processings/' . $processing_id, array(), 'GET');
        $array_processing = [];

        if (isset($processing->data->projectId) && $processing->data->projectId == self::$current_project) {
            $array_processing['id'] = $processing->id;
            $array_processing['name'] = $processing->data->name;
        }

        return $array_processing;
    }

    public function createWebHook()
    {
        update_option('vnca_axeptio_webhook_id', null);
        $this->deleteWebHook(self::$webhook_id);

        $processings = self::$api_connection->call('webhooks', [
            "uri" => Controller::getRoute("webhook_axeptio_response"),
            "event" => "app/consent/" . get_option('vnca_axeptio_id'),
            "headers" => [
                "project_id" => get_option('vnca_axeptio_id')
            ]
        ], 'POST');

        if (empty($processings->_id)) {
            return -1;
        }

        update_option('vnca_axeptio_webhook_id', $processings->_id);
        return (isset($processings->_id)) ? $processings->_id : -1;
    }

    public function deleteWebHook($webhook_id)
    {
        if(!empty($webhook_id)){
            $processings = self::$api_connection->call('webhooks/' . $webhook_id, [], 'DELETE');
            return (isset($processings->result->n)) ? true : false;
        } else {
            return true;
        }
    }

//    -------------------  Concents information -------------------

    /**
     * @param $user ConsentementEntity
     */
    public function getUserConsentInformation($user){
        return Consents::getInstance()->getUserConsentInformation($user);
    }


    public function getValidateConsentByWidget($widget_id){
        return Consents::getInstance()->getValidateConsentByWidget($widget_id);
    }

}