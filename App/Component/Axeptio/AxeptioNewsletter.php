<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 19/11/2019
 * Time: 15:42
 */

namespace vnca_axeptio\App\Component\Axeptio;

use vnca_axeptio\App\Api\Api;
use vnca_axeptio\App\Api\Controller\Controller;
use vnca_axeptio\App\Api\Services\TokenServices;
use vnca_axeptio\Core\Newsletter\Fact_Newsletter;
use vnca_axeptio\Core\Singleton;
use vnca_axeptio\App\Entity\ConsentementEntity;
use vnca_axeptio\App\Entity\NewsletterUnsubscribeEntity;
use vnca_axeptio\App\Repository\ConsentementRepository;

class AxeptioNewsletter
{
    use Singleton;

    public function subscribeUser($token, $identifier){

        if($identifier != get_option('newsletter_system_widget')){
            return false;
        }

        $user = ConsentementRepository::getInstance()->getUserByToken($token);


        if($user != null && get_class($user) == ConsentementEntity::class){
           //file_put_contents(VNCA_DIR . 'logs/axeptiov2.log', '[' . date('d/m/Y - H:i:s') . '] ' . print_r($user, true) . PHP_EOL, FILE_APPEND | LOCK_EX);

            Fact_Newsletter::getInstance()->createUser($user);
        }
    }

    public function unsubscribeUser($token, $identifier){

        if($identifier != get_option('newsletter_system_widget')){
            return false;
        }

        $user = ConsentementRepository::getInstance()->getUserByToken($token);


        if($user != null && get_class($user) == ConsentementEntity::class){
            Fact_Newsletter::getInstance()->deleteUser($user);
        }
    }

    public function useWasUnsubscribe($email, $source){

        global $wp;

        $token = ConsentementRepository::getInstance()->getTokenUserByEmail($email);
        $project_id = get_option('vnca_axeptio_id');
        $newsletter_widget = get_option('newsletter_system_widget');

        if ($token != ''){
            $unsubscribe = Connection::getInstance()->call('app/consents/'.$project_id.'/processings/'. $newsletter_widget, [
                "accept" => false,
                "preferences" => [],
                "token" => $token
            ],'POST',[
                "origin" => $source,
                "referer" => home_url($wp->request),
            ]);

            if (isset($unsubscribe->_id)){
                $id = $unsubscribe->_id;
                $register_entity = new NewsletterUnsubscribeEntity();
                $register_entity
                    ->setTokenUser($token)
                    ->setIdentifierAxeptio($id)
                    ->setSource($source);
                $response = $register_entity->flush();

                return (is_integer($response) && $response > 0);
            }
            return false;
        }
        return false;
    }
}