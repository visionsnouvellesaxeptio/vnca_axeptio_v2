<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 18/10/2019
 * Time: 12:10
 */

namespace vnca_axeptio\App\Component\Shortcode;


use vnca_axeptio\App\Component\Axeptio\Axeptio;
use vnca_axeptio\Core\Newsletter\Fact_Newsletter;
use vnca_axeptio\Core\Renderer;
use vnca_axeptio\Core\Singleton;

class FormNewsletterShortcode
{
    use Singleton;

    private static $instance = null;
    public static $nb_shortcode = 0;
    public static $name_shortcode = 'vnca_form_newsletter';


    private function __instance()
    {
        add_action('wp_enqueue_scripts', array($this, 'wp_enqueue_scripts'));
        $this->add_shortcode(self::$name_shortcode, array($this, 'init'));
    }

    public function init($attributes)
    {
        $newsletter_name = Fact_Newsletter::getCurrentNameNewsletter();
        $box_newsletter = Axeptio::getInstance()->getProcessingById(get_option('newsletter_system_widget'));

        if(empty($box_newsletter['name'])){
           if (is_admin()){
               return __('Admin: No widget found for newsletter please check in the back office', VNCA_TEXTDOMAIN);
           }
           return '';
        }

        $return = '';
        try {
            $description = get_option('vnca_'. Fact_Newsletter::getInstance()->getName() .'_description');
            $attributes = shortcode_atts(
                array(
                    'id' => '',
                    'axeptio' => $box_newsletter,
                    'description' => $description
                ),
                $attributes,
                self::$name_shortcode
            );

            if(!empty($newsletter_name)){
                $return = Renderer::render('/shortcode/newsletter/form/' . $newsletter_name . '.php', $attributes);
            }
        } catch (\Exception $e){
            $return = '';
        }


        return $return;
    }

    public function wp_enqueue_scripts()
    {
    }

    private function add_shortcode($tag, $func)
    {
        add_shortcode($tag, $func);
    }
}
