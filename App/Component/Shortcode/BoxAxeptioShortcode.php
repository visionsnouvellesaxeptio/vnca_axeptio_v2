<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 18/10/2019
 * Time: 12:10
 */

namespace vnca_axeptio\App\Component\Shortcode;

use vnca_axeptio\App\Api\Controller\Controller;
use vnca_axeptio\App\Component\Axeptio\SDK;
use vnca_axeptio\Core\Renderer;
use vnca_axeptio\Core\Singleton;
use vnca_axeptio\App\PageSetting\OptionScripts;

class BoxAxeptioShortcode
{
    use Singleton;

    private static $instance = null;
    public static $nb_shortcode = 0;
    public static $name_shortcode = 'vnca_box_axeptio';
    private $axeptio_sdk = null;

    private function __instance()
    {
        $this->axeptio_sdk = SDK::getInstance();
        add_action('wp_enqueue_scripts', array($this, 'wp_enqueue_scripts'));
        $this->add_shortcode(self::$name_shortcode, array($this, 'init'));
    }

    public function init($attributes)
    {
        $this->axeptio_sdk->loadSDK();

        if (is_object($attributes) && get_class($attributes) == "WPCF7_FormTag") {
            $attributes = $this->loadWPCF7Options($attributes);
        }

        self::$nb_shortcode += 1;
        $attributes = shortcode_atts(
            array(
                'id' => 'RGPD_contact',
                'email' => '',
                'required' => 'true'
            ),
            $attributes,
            self::$name_shortcode
        );

        $attributes["id_consentement"] = $attributes["id"];
        $attributes["axeptio_token"] = "axeptio_token_" . self::$nb_shortcode;
        $attributes["check_consent"] = "check_consent_" . self::$nb_shortcode;
        $attributes["widget_axeptio_container"] = 'widget_axeptio_container_' . self::$nb_shortcode;
        $attributes["id_project"] = get_option('vnca_axeptio_id');
        $attributes["email_id"] = $attributes["email"];
        $attributes["id"] = self::$nb_shortcode;

        $attributes["routes"]['get_consent'] = Controller::getRoute('get_consent');
        $attributes["routes"]['save_consent'] = Controller::getRoute('save_consent');

        wp_enqueue_script('box_axeptio_js');
        wp_enqueue_style('style_form_validation');
        wp_enqueue_style('style_alertify');
        wp_enqueue_script('script_alertify');

        return Renderer::render('/shortcode/boxAxeptio.php', $attributes);
    }

    public function wp_enqueue_scripts()
    {
        $sdk = $this->axeptio_sdk->register();
        wp_register_script('box_axeptio_js', VNCA_URL . 'assets/js/axeptio/box_axeptio.js', $sdk, PLUGIN_VERSION, true);

        OptionScripts::getInstance()->registerIfNeeded('style_form_validation');
        OptionScripts::getInstance()->registerIfNeeded('style_alertify');
        OptionScripts::getInstance()->registerIfNeeded('script_alertify');
    }

    private function add_shortcode($tag, $func)
    {
        add_shortcode($tag, $func);
        if (function_exists('wpcf7_add_form_tag')) {
            wpcf7_add_form_tag($tag, $func);
        } elseif (function_exists('wpcf7_add_shortcode')) {
            wpcf7_add_shortcode($tag, $func);
        }
    }

    private function loadWPCF7Options($attributes)
    {
        $attr = [];

        $temp_attributes = $attributes;
        if (isset($attributes['options']) && is_array($attributes['options']) && !empty($attributes['options'])) {
            $temp_attributes = $attributes['options'];
        } elseif ((isset($attributes['attr']) && $attributes['attr'] != "")) {
            $temp_attributes = explode('" ', $attributes['attr']);
        }

        if (isset($temp_attributes) && is_array($temp_attributes)) {
            foreach ($temp_attributes as $option) {
                $temp_options = explode(':', $option);
                if (sizeof($temp_options) == 2) {
                    $attr[$temp_options[0]] = str_replace('"', '', $temp_options[1]);
                }
            }
        }

        return $attr;
    }
}