<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 23/10/2019
 * Time: 17:12
 */

namespace vnca_axeptio\App\Api;

use vnca_axeptio\App\Api\Controller\Controller;

class Api
{

    private static $instance = null;

    private function __construct()
    {}

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Api();
        }
        return self::$instance;
    }

    function execute(){
        new Controller();
    }

}