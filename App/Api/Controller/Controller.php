<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 23/10/2019
 * Time: 17:28
 */

namespace vnca_axeptio\App\Api\Controller;


use vnca_axeptio\App\Api\Services\NewsletterServices;
use vnca_axeptio\App\Api\Services\TokenServices;
use vnca_axeptio\App\Api\Services\WebHookServices;


class Controller extends \WP_REST_Controller
{
    static $base = '/';
    static $initName = false;
    static $routes;

    public function __construct($initNameRoute = false)
    {
        self::$initName = $initNameRoute;
        $this->init();
        self::$initName = false;
    }

    private function init(){

        $this->register_rest_route( '/consent/token', 'get_consent', array(
            array(
                'methods'             => \WP_REST_Server::READABLE, //GET
                'callback'            => array( TokenServices::getInstance(), 'get_token_by_mail'),
                'permission_callback' => '__return_true'
            ),
        ) );

        $this->register_rest_route( '/consent/token', 'save_consent', array(
            array(
                'methods'             => \WP_REST_Server::CREATABLE, //POST
                'callback'            => array( TokenServices::getInstance(), 'save_token'),
                'permission_callback' => '__return_true'
            ),
        ) );

        $this->register_rest_route( '/consent/export', 'export_consents', array(
            array(
                'methods'             => \WP_REST_Server::CREATABLE, //POST
                'callback'            => array( TokenServices::getInstance(), 'export_consents'),
                'permission_callback' => '__return_true'
            ),
        ));

        $this->register_rest_route( '/webhook/Axeptio', 'delete_webhook_axeptio', array(
            array(
                'methods'             => \WP_REST_Server::DELETABLE, //DELETE
                'callback'            => array( WebHookServices::getInstance(), 'delete_webhook_axeptio'),
                'permission_callback' => '__return_true'
            ),
        ));

        $this->register_rest_route( '/webhook/axeptio', 'create_webhook_axeptio', array(
            array(
                'methods'             => \WP_REST_Server::CREATABLE, //POST
                'callback'            => array( WebHookServices::getInstance(), 'create_webhook_axeptio'),
                'permission_callback' => '__return_true'
            ),
        ));

        $this->register_rest_route( '/webhook/newsletter/axeptio/subscribe', 'webhook_axeptio_response', array(
            array(
                'methods'             => \WP_REST_Server::CREATABLE, //POST
                'callback'            => array( WebHookServices::getInstance(), 'webhook_axeptio_response'),
                'permission_callback' => '__return_true'
            ),
        ));

        $this->register_rest_route( '/webhook/newsletter/unsubscribe', 'webhook_newsletter_unsubscribe_response', array(
            array(
                'methods'             => \WP_REST_Server::CREATABLE, //POST
                'callback'            => array( NewsletterServices::getInstance(), 'webhook_newsletter_unsubscribe_response'),
                'permission_callback' => '__return_true'
            ),
        ));

        $this->register_rest_route( '/newsletter/validate', 'newsletter_validate_connexion_and_initialize', array(
            array(
                'methods'             => \WP_REST_Server::CREATABLE, //POST
                'callback'            => array( NewsletterServices::getInstance(), 'newsletter_validate_connexion_and_initialize'),
                'permission_callback' => '__return_true'
            ),
        ));
    }

    public function register_rest_route($route, $route_name, $args = array(), $override = false){
        self::$routes[$route_name] = $route;
        if (! self::$initName){
            register_rest_route( PLUGIN_API_PATH, self::$base . $route, $args, $override);
        }
    }

    /**
     * init the name when the routes is empty, the route is only load when the api is running
     *
     * @param $route
     * @return string|null
     */
    public static function getRoute($route){
        if (empty(self::$routes)){
            new Controller(true);
        }
        if(isset(self::$routes[$route])){
            return  get_home_url() . '/wp-json/' . PLUGIN_API_PATH . self::$routes[$route];
        } else {
            return get_home_url();
        }
    }
}