<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 24/10/2019
 * Time: 16:27
 */

namespace vnca_axeptio\App\Api\Services;


class Services
{

    private static $instance = null;

    private function __construct(){}

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Services();
        }
        return self::$instance;
    }

    /**
     * Allow to format response mor easily
     * @param $data
     * @param $code
     * @param array $header
     * @param null $error
     * @return \WP_REST_Response
     */
    public function response($data, $code, $header=[], $error = null){
        $return = [];
        if ($error !== null){
            $return['identifier'] = $error;
            $return['error'] = $data;
            $return['code'] = $code;
        }else{
            $return['data'] = $data;
            $return['code'] = $code;
            $return['header'] = $header;
        }

        return new \WP_REST_Response($return, $code, $header);
    }

}