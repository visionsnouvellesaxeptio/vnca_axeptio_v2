<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 24/10/2019
 * Time: 16:27
 */

namespace vnca_axeptio\App\Api\Services;

use vnca_axeptio\Core\Newsletter\Fact_Newsletter;
use vnca_axeptio\Core\Singleton;

class NewsletterServices extends Services
{
    use Singleton;

    private function __instance(){
    }

    public function newsletter_validate_connexion_and_initialize($request)
    {
        if (empty($request['newsletter_system'])){
            return $this->response( [
                "message" => 'bad parameters',
            ], 400, '' ,'bad_parameters');
        }

        $newsletter_name = $request['newsletter_system'];
        $newsletter = Fact_Newsletter::getInstance($newsletter_name);

        if ($newsletter == null){
            return $this->response( [
                "message" => __('The newsletter system not exist', VNCA_TEXTDOMAIN),
            ], 400, '', 'newsletter_not_exist');
        }
        $is_authentificate = $newsletter->isValideConnexion($request);

        if (!$is_authentificate){
            return $this->response(
                [
                    'success' => 0,
                    'message' => __('Authentification failed', VNCA_TEXTDOMAIN)
                ]
                , 401, '','autentification_failed'
            );
        } else {
            update_option('newsletter_system', $newsletter_name);
            $newsletter->initialized();

            return $this->response(
                [
                    'success' => 1,
                    'message' => __('Connection to the newsletter system has been completed', VNCA_TEXTDOMAIN)
                ]
                , 200
            );
        }
    }

    public function webhook_newsletter_unsubscribe_response($request){
        $newsletter = Fact_Newsletter::getInstance();
        $response = $newsletter->userUnsubscribe($request);
        if ($response){
           $message = __('The user has correctly unsubscribe', VNCA_TEXTDOMAIN);
        } else {
           $message = __('Unsubscription to the newsletter could not be applied, please contact the site manager', VNCA_TEXTDOMAIN);
        }
        return $this->response(
            [
                'success' => $response,
                'message' => $message
            ]
            , 200
        );
    }
}