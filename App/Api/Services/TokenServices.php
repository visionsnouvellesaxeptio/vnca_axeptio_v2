<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 24/10/2019
 * Time: 16:27
 */

namespace vnca_axeptio\App\Api\Services;

use vnca_axeptio\App\Services\Consentement\ExportConsentementServices;
use vnca_axeptio\Core\Singleton;
use vnca_axeptio\App\Repository\ConsentementRepository;
use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

class TokenServices extends Services
{

    use Singleton;

    /**
     * @var ConsentementRepository
     */
    private static $ConsentementRepository = null;

    private function __instance()
    {
        self::$ConsentementRepository = ConsentementRepository::getInstance();
    }

    /**
     * get the tocket of email
     *
     * @param WP_REST_Request $request Full data about the request.
     * @return WP_Error|WP_REST_Response
     */
    public function get_token_by_mail($request)
    {

        if (!isset($request['email'])) {
            return $this->response([
                "message" => 'bad parameters'
            ], 400, '','bad_parameters');
        }

        $tockenUser = self::$ConsentementRepository->getTokenUserByEmail($request['email']);

        if (empty($tockenUser)) {
            return $this->response([
                "message" => 'User not exist'
            ], 200);
        }

        return $this->response(['token' => $tockenUser], 200);
    }

    /**
     * update or insert email with token
     *
     * @param WP_REST_Request $request Full data about the request.
     * @return WP_Error|WP_REST_Response
     */
    public function save_token($request)
    {
        if (!isset($request['email']) || !isset($request['token'])) {
            return $this->response([
                "message" => 'bad parameters',
            ], 400, '','bad_parameters');
        }

        $tockenUser = self::$ConsentementRepository->saveTokenForEmail($request['email'], $request['token']);

        return $this->response($tockenUser, 200);
    }

    /**
     * @param $request
     * @return WP_REST_Response
     */
    public function export_consents($request)
    {
        $email = (isset($request['email'])) ? $request['email'] : '';
        $widget_id = (isset($request['widget_name'])) ? $request['widget_name'] : '';
        $only_valid = isset($request['valid_consent']);

        try {
            $csv_base64 = ExportConsentementServices::getInstance()->getExportCsv($email, $widget_id, $only_valid);
        } catch (\Exception $e) {
            return $this->response([
                'message'=> $e->getMessage()
            ], 200, 'application/json');
        }

        return $this->response([
            'consentment_file_base64'=> "data:application/octet-stream;base64," . $csv_base64,
            'consentment_filename'=> 'axeptio-export-consents.csv'
        ], 200, 'application/json');
    }
}