<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 24/10/2019
 * Time: 16:27
 */

namespace vnca_axeptio\App\Api\Services;

use vnca_axeptio\App\Component\Axeptio\Axeptio;
use vnca_axeptio\App\Component\Axeptio\AxeptioNewsletter;
use vnca_axeptio\Core\Singleton;
use vnca_axeptio\App\Repository\ConsentementRepository;

class WebHookServices extends Services
{
    use Singleton;

    private static $ConsentementRepository = null;

    /**
     * use like construct instance
     */
    private function __instance()
    {
        self::$ConsentementRepository = ConsentementRepository::getInstance();
    }

    public function delete_webhook_axeptio()
    {
        $response = Axeptio::getInstance()->deleteWebHook(get_option('vnca_axeptio_webhook_id'));
        return $this->response($response, 200);
    }

    public function create_webhook_axeptio()
    {
        $response = Axeptio::getInstance()->createWebHook();
        return $this->response($response, 200);
    }

    public function webhook_axeptio_response($request)
    {
//        file_put_contents(VNCA_DIR . 'logs/axeptiov2.log', '[' . date('d/m/Y - H:i:s') . '] ' . print_r($request, true) . PHP_EOL, FILE_APPEND | LOCK_EX);
        $response = '';
        if (isset($request['token']) && isset($request['accept']) && $request['accept'] == 1 && isset($request['identifier'])) {
            $response = 'created';
            AxeptioNewsletter::getInstance()->subscribeUser($request['token'], $request['identifier']);

        } elseif (isset($request['token']) && isset($request['accept']) && isset($request['identifier'])) {
            $response = 'deleted';
            AxeptioNewsletter::getInstance()->unsubscribeUser($request['token'], $request['identifier']);

        }

        return $this->response(['res' => $response ], 200);
    }
}