<?php

namespace vnca_axeptio\App\Repository;

use vnca_axeptio\App\Entity\ConsentementEntity;

final class ConsentementRepository
{

    private $name = 'vnca_consentement_RGPD';
    static $instance = null;

    protected function __construct()
    {
        global $wpdb;
        $this->name = $wpdb->prefix . $this->name;
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new ConsentementRepository();
        }
        return self::$instance;
    }

    /**
     * Create table initialized in activation plugin
     */
    public function createTable()
    {
        $sql = "CREATE TABLE $this->name (
                id_consentement int(11) NOT NULL AUTO_INCREMENT,
                email varchar(255) NOT NULL UNIQUE,
                token varchar(255) NOT NULL UNIQUE,
                PRIMARY KEY (id_consentement)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
              ";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        \dbDelta($sql);
    }

    /**
     * @param $email
     * @return string
     */
    function getTokenUserByEmail($email)
    {
        $user = self::getUserByEmail($email);
        return ($user != null && get_class($user) == ConsentementEntity::class)? $user->getToken() : '';
    }

    /**
     * @param $token
     * @return string
     */
    function getEmailByToken($token)
    {
        global $wpdb;
        $resultats = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT email FROM {$this->name} WHERE token = %s",
                $token
            )
        );

        return (!empty($resultats) && isset($resultats[0]->email)) ? $resultats[0]->email : null;
    }

    /**
     * @param $token
     * @return ConsentementEntity
     */
    function getUserByToken($token)
    {
        global $wpdb;
        $resultats = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM {$this->name} WHERE token = %s",
                $token
            )
        );

        return (sizeof($resultats) > 0) ? ConsentementEntity::hydrate($resultats[0]) : null;
    }

    /**
     * @param $email
     * @return ConsentementEntity|null
     */
    function getUserByEmail($email)
    {
        global $wpdb;
        $resultats = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM {$this->name} WHERE email = %s",
                $email
            )
        );

        return (sizeof($resultats) > 0) ? ConsentementEntity::hydrate($resultats[0]) : null;
    }

    function saveTokenForEmail($email, $token)
    {
        global $wpdb;

        $resultats = [];
        $user = $this->getUserByEmail($email);

        if ($user == null || empty($user)) {
            $resultats = $wpdb->insert(
                $this->name,
                array(
                    'email' => $email,
                    'token' => $token
                )
            );
        }

        return $resultats;
    }

    /**
     * @return ConsentementEntity[]
     */
    function getAll()
    {

        global $wpdb;
        $resultats = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM {$this->name}"
            )
        );

        return (sizeof($resultats) > 0) ? ConsentementEntity::hydrateAll($resultats) : null;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}