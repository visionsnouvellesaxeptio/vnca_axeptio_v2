<?php

namespace vnca_axeptio\App\Repository;

use vnca_axeptio\App\Entity\NewsletterUnsubscribeEntity;

final class NewsletterUnsubscribeRepository
{

    private $name = 'vnca_newsletter_unsubscribe';
    static $instance = null;

    protected function __construct()
    {
        global $wpdb;
        $this->name = $wpdb->prefix . $this->name;
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new NewsletterUnsubscribeRepository();
        }
        return self::$instance;
    }

    public function createTable()
    {
        $consent_name = ConsentementRepository::getInstance()->getName();
        $sql = "CREATE TABLE $this->name (
                id int(255) NOT NULL AUTO_INCREMENT,
                identifier_axeptio varchar(255) NOT NULL,
                token_user varchar(255) NOT NULL,
                source varchar(255) NOT NULL,
                date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (id),
                FOREIGN KEY (token_user) REFERENCES ". $consent_name."(token)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
              ";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        \dbDelta($sql);
    }


    public function add(NewsletterUnsubscribeEntity $entity){
        global $wpdb;

        $resultats = $wpdb->insert(
            $this->name,
            [
                'token_user' => $entity->getTokenUser(),
                'source' => $entity->getSource(),
                'identifier_axeptio' => $entity->getIdentifierAxeptio()
            ]
        );

        return $resultats;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}