<?php

namespace vnca_axeptio\App\Repository;

use vnca_axeptio\App\Entity\WebHookEntity;

final class WebHookRepository
{

    private $name = 'vnca_webhook';
    static $instance = null;

    protected function __construct()
    {
        global $wpdb;
        $this->name = $wpdb->prefix . $this->name;
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new WebHookRepository();
        }
        return self::$instance;
    }

    public function createTable()
    {
        $sql = "CREATE TABLE $this->name (
                id_newsletter int(255) NOT NULL AUTO_INCREMENT,
                description varchar(255) NOT NULL,
                newsletter_name varchar(255) NOT NULL,
                PRIMARY KEY (id_newsletter)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
              ";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        \dbDelta($sql);
    }

    function addWebHook(WebHookEntity $webHookEntity)
    {
        global $wpdb;

        $result = $wpdb->insert(
            $this->name,
            array(
               'id_newsletter' => $webHookEntity->getIdNewsletter(),
               'description' => $webHookEntity->getDescription(),
               'newsletter_name' => $webHookEntity->getNewslettername(),
            )
        );

        return $result;
    }

    function deleteWebHook(WebHookEntity $webHookEntity)
    {
        global $wpdb;

        $result = $wpdb->delete(
            $this->name,
            array(
                'id_newsletter' => $webHookEntity->getIdNewsletter()
            )
        );

        return $result;
    }

    function getHookByNewsletter($newsletter_name){
        global $wpdb;
        $resultats = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM {$this->name} WHERE newsletter_name = %s",
                $newsletter_name
            )
        );
        return (sizeof($resultats) > 0 )? WebHookEntity::hydrateAll($resultats) : [];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}