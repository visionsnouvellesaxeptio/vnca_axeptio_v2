<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 21/10/2019
 * Time: 09:44
 */

namespace vnca_axeptio\App\PageSetting;


use vnca_axeptio\App\Api\Controller\Controller;
use vnca_axeptio\App\Component\Axeptio\Axeptio;
use vnca_axeptio\App\Component\Newsletter\SendinBlue\SendinBlue;
use vnca_axeptio\Core\Renderer;

class GeneralOption
{
    private static $instance = null;
    private static $name_option = 'vnca_axeptio_options';
    private static $hash_key = 'f56545edf579a251c7c50c4fca98dbc8';

    private function __construct()
    {
        add_action('admin_enqueue_scripts', array($this, 'loadScriptOption'));
        add_action('admin_menu', array($this, 'execute'));
    }

    public function execute()
    {
        //create new top-level menu
        add_menu_page(
            __('GDPR consents', VNCA_TEXTDOMAIN),
            __('GDPR consents', VNCA_TEXTDOMAIN),
            'manage_options',
            __CLASS__,
            array($this, 'plugin_settings_page')
        );

        //call register settings function
        add_action('admin_init', array($this, 'register_plugin_settings'));
    }

    function register_plugin_settings()
    {
        //register our settings
        register_setting(self::$name_option, 'vnca_axeptio_id');
        register_setting(self::$name_option, 'vnca_axeptio_email');
        register_setting(self::$name_option, 'vnca_axeptio_password');
        register_setting(self::$name_option, 'vnca_axeptio_text');
        register_setting(self::$name_option, 'newsletter_system_widget');
        register_setting(self::$name_option, 'newsletter_system');
        register_setting(self::$name_option, 'vnca_sendinblue_token');
        register_setting(self::$name_option, 'vnca_' . SendinBlue::getName() . '_description');
        register_setting(self::$name_option, 'vnca_axeptio_sdk_in_all_pages');
        register_setting(self::$name_option, 'vnca_cookie_version');

        $this->encriptSettingOptions('vnca_axeptio_password');
    }

    private function encriptSettingOptions($option)
    {
        add_filter("pre_update_option_" . $option, [$this, 'encryptOption'], 10, 3);
    }

    public static function encryptOption($value, $old_value, $option)
    {
        $value = openssl_encrypt($value, "AES-128-ECB", self::$hash_key);
        add_option($option . '_encrypt', "1");
        return $value;
    }

    function plugin_settings_page()
    {
        wp_enqueue_style('vnca_general_option_style');
        wp_enqueue_script('vnca_download');
        wp_enqueue_script('vnca_general_option_script');
        wp_enqueue_script('vnca_newsletter_option_script');

        echo Renderer::render('/admin/options.php', [
            'name_option' => self::$name_option,
            'processings' => Axeptio::getInstance()->getAllProcessing()
        ]);
    }

    public function loadScriptOption()
    {
        $nonce = wp_create_nonce('wp_rest');
        wp_register_style('vnca_general_option_style', VNCA_URL . 'assets/css/general_option_style.css', '', PLUGIN_VERSION);
        wp_register_script('vnca_download', VNCA_URL . 'assets/js/component/download.js', '', PLUGIN_VERSION);
        wp_register_script('vnca_general_option_script', VNCA_URL . 'assets/js/options/option.js', ['vnca_download'], PLUGIN_VERSION);
        wp_register_script('vnca_newsletter_option_script', VNCA_URL . 'assets/js/options/option_newsletter.js', ['vnca_download'], PLUGIN_VERSION);

        $API_CALL = [
            'create_webhook_axeptio' => Controller::getRoute('create_webhook_axeptio'),
            'delete_webhook_axeptio' => Controller::getRoute('delete_webhook_axeptio'),
            'newsletter_validate_connexion_and_initialize' => Controller::getRoute('newsletter_validate_connexion_and_initialize'),
            'export_consents' => Controller::getRoute('export_consents'),
            'nonce' => $nonce
        ];
        wp_localize_script('vnca_newsletter_option_script', 'API_CALL', $API_CALL);
        wp_localize_script('vnca_general_option_script', 'API_CALL', $API_CALL);
    }

    public function getAxeptioPassword()
    {
        $password = get_option('vnca_axeptio_password');
        if (get_option('vnca_axeptio_password_encrypt') == 1) {
            $password = openssl_decrypt(get_option('vnca_axeptio_password'), "AES-128-ECB", $this::$hash_key);
        } else {
            $password = get_option('vnca_axeptio_password');
        }
        return $password;
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new GeneralOption();
        }
        return self::$instance;
    }


    public function getSdkInAllPages()
    {
        $load_sdk_in_all_pages = get_option('vnca_axeptio_sdk_in_all_pages', false);
        return ($load_sdk_in_all_pages === 1) ? true : $load_sdk_in_all_pages;
    }
}