<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 21/10/2019
 * Time: 09:44
 */

namespace vnca_axeptio\App\PageSetting;


use vnca_axeptio\App\Api\Controller\Controller;
use vnca_axeptio\App\Component\Axeptio\Axeptio;
use vnca_axeptio\Core\Renderer;
use vnca_axeptio\Core\Singleton;

define('OPTION_SCRIPT_STYLE_FORM_VALIDATION_LABEL', __('Form validation bootstrap (Style)', VNCA_TEXTDOMAIN));
define('OPTION_SCRIPT_STYLE_FORM_VALIDATION_DESCRIPTION', __('Informs the user if he has not correctly completed the form', VNCA_TEXTDOMAIN));

define('OPTION_SCRIPT_STYLE_ALERTIFY_LABEL', __('Alertify.js (Style)', VNCA_TEXTDOMAIN));
define('OPTION_SCRIPT_STYLE_ALERT_DESCRIPTION', __('Notified the user according to the steps (Style).', VNCA_TEXTDOMAIN));

define('OPTION_SCRIPT_SCRIPT_ALERTIFY_LABEL', __('Alertify.js (Script)', VNCA_TEXTDOMAIN));
define('OPTION_SCRIPT_SCRIPT_ALERT_DESCRIPTION', __('Notified the user according to the steps (Script).', VNCA_TEXTDOMAIN));

class OptionScripts
{
    use Singleton;
    private static $instance = null;
    private static $name_option = 'vnca_axeptio_options_scripts';

    private static $script = [
        'style_form_validation' => [
            'type' => 'style',
            'url' => VNCA_SCRIPT_NEEDED . 'css/bootstrap_form_validation.css',
            'label' => OPTION_SCRIPT_STYLE_FORM_VALIDATION_LABEL,
            'description' => OPTION_SCRIPT_STYLE_FORM_VALIDATION_DESCRIPTION,
            'use_cdn' => false
        ],
        'style_alertify' => [
            'type' => 'style',
            'url' => VNCA_SCRIPT_NEEDED . 'css/alertify.min.css',
            'cdn' => '//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css',
            'label' => OPTION_SCRIPT_STYLE_ALERTIFY_LABEL,
            'description' => OPTION_SCRIPT_STYLE_ALERT_DESCRIPTION,
            'use_cdn' => true,
            'use_custom_cdn' => true
        ],
        'script_alertify' => [
            'type' => 'script',
            'url' => VNCA_SCRIPT_NEEDED . 'js/alertify.min.js',
            'cdn' => '//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js',
            'label' => OPTION_SCRIPT_SCRIPT_ALERTIFY_LABEL,
            'description' => OPTION_SCRIPT_SCRIPT_ALERT_DESCRIPTION,
            'dependencies' => 'jquery',
            'in_footer' => false,
            'use_cdn' => true,
            'use_custom_cdn' => true
        ]
    ];

    private function __instance()
    {
        add_action('admin_menu', array($this, 'execute'));
    }

    public function execute()
    {
        //create new top-level menu
        add_submenu_page(
            GeneralOption::class,
            __('Manage scripts', VNCA_TEXTDOMAIN),
            __('Manage scripts', VNCA_TEXTDOMAIN),
            'manage_options',
            __CLASS__,
            array($this, 'plugin_settings_page')
        );

        //call register settings function
        add_action('admin_init', array($this, 'register_plugin_settings'));
    }

    function register_plugin_settings()
    {
        //register our settings
        foreach (self::$script as $name => $option) {
            register_setting( self::$name_option, $name);
            register_setting( self::$name_option, $name.'_use_cdn');
            register_setting( self::$name_option, $name.'_use_custom_cdn');
        }
    }

    function plugin_settings_page()
    {
        wp_enqueue_style('general_option_style');

        echo Renderer::render('/admin/optionsScripts.php', [
            'name_option' => self::$name_option,
            'scripts' => self::$script
        ]);
    }

    public function loadScriptOption(){
        wp_register_style('general_option_style', VNCA_URL . 'assets/css/general_option_style.css', '', PLUGIN_VERSION);
    }

    /**
     * @param $handle
     * @return String|null
     */
    public function userNeedScript($handle){
        $script_slug = $handle;
        $handle = (!empty(self::$script[$handle]))? self::$script[$handle] : null;

        if (!empty($script_slug) && !empty(get_option($script_slug))){
            return (!empty($handle['url']))? $handle['url'] : null;
        }

        return null;
    }

    /**
     * Allow to register script if user need
     * @param $handle
     * @return bool
     */
    public function registerIfNeeded($handle){
        if(empty($this->userNeedScript($handle))){
            return false;
        }

        $script_dep = (!empty(self::$script[$handle]['dependencies']))? self::$script[$handle]['dependencies'] : '';
        $type = (!empty(self::$script[$handle]['type']))? self::$script[$handle]['type'] : null;
        $url = self::getTheScriptUrl($handle);
        $footer = (!empty(self::$script[$handle]['footer']))? self::$script[$handle]['footer'] : false;


        if(empty($type) || empty($url)){
            return false;
        }

        if ($type === 'style'){
            wp_register_style($handle, $url, $script_dep, PLUGIN_VERSION);
        }else{
            wp_register_script($handle, $url, $script_dep, PLUGIN_VERSION, $footer);
        }

        return true;
    }

    /**
     * @param $handle
     * @return false|mixed|void
     */
    private function getTheScriptUrl($handle){
        $use_cdn = !empty(get_option($handle .'_use_cdn'))? get_option($handle .'_use_cdn') : null;
        $cdn_custom = !empty(get_option($handle .'_use_custom_cdn'))? get_option($handle .'_use_custom_cdn') : null;

        if (!empty($use_cdn) && !empty($cdn_custom)){
            return $cdn_custom;
        } else if ($use_cdn && !empty(self::$script[$handle]['cdn'])) {
            return self::$script[$handle]['cdn'];
        } else {
            return self::$script[$handle]['url'];
        }
    }
}