<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 26/09/2019
 * Time: 18:14
 */

namespace vnca_axeptio\App;

use vnca_axeptio\App\Component\Axeptio\SDK;
use vnca_axeptio\App\Component\Shortcode;
use vnca_axeptio\App\Component\Widget;
use vnca_axeptio\App\PageSetting\GeneralOption;
use vnca_axeptio\App\PageSetting\OptionScripts;
use vnca_axeptio\App\Repository\ConsentementRepository;
use vnca_axeptio\App\Repository\NewsletterUnsubscribeRepository;
use vnca_axeptio\App\Repository\WebHookRepository;

class App
{
    private static $instance = null;

    private function __construct(){
        load_plugin_textdomain( VNCA_TEXTDOMAIN, false, VNCA_FOLDER . 'languages' );
        add_action('wp_enqueue_scripts', array($this, 'wp_enqueue_scripts'));
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new App();
        }
        return self::$instance;
    }

    function execute(){
        GeneralOption::getInstance();
        OptionScripts::getInstance();
        Shortcode::getInstance();
        Widget::getInstance()->init();
    }

    function activation()
    {
        ConsentementRepository::getInstance()->createTable();
        WebHookRepository::getInstance()->createTable();
        NewsletterUnsubscribeRepository::getInstance()->createTable();
    }

    function wp_enqueue_scripts(){
        wp_register_style('general_style', VNCA_URL . 'assets/style.css', '', PLUGIN_VERSION, false);
        wp_enqueue_style( 'general_style' );
        if(GeneralOption::getInstance()->getSdkInAllPages()) {
            $sdk = SDK::getInstance();
            $enqueue_sdk = $sdk->register();
            $sdk->loadSDK();
        }
    }

    function deactivation(){}

    function uninstall(){}
}