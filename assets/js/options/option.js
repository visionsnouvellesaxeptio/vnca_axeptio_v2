jQuery(document).ready(function () {
    let select_newsletter_system = jQuery('#select_newsletter_system');
    let select_newsletter_system_content = jQuery('#select_newsletter_system_content');
    let select_newsletter_system_widget = jQuery('#select_newsletter_system_widget');
    let target_before_change = null;

    select_newsletter_system_widget.on('change', function (event) {
        loadSelectedNewsletterSystemWidget();
    });

    select_newsletter_system.on('change', function (event) {
        loadSelectedNewsletterSystem();
    });

    function loadSelectedNewsletterSystemWidget() {
        let option_select = select_newsletter_system_widget.val();

        if(option_select !== ''){
            select_newsletter_system_content.show();
            loadSelectedNewsletterSystem();
        }else{
            select_newsletter_system_content.hide();
            select_newsletter_system.val('');
            select_newsletter_system.trigger('change');
        }
    }

    function loadSelectedNewsletterSystem() {
        let option_select = select_newsletter_system.val();
        let target_cutrent_select = jQuery('#' + option_select);

        if (target_before_change !== null) {
            target_before_change.hide();
        }

        target_cutrent_select.show();
        target_before_change = target_cutrent_select;
    }
    loadSelectedNewsletterSystemWidget();
});

jQuery(document).ready(function () {
    let form_export = jQuery('#export_consents');

    if (API_CALL != undefined && form_export.length > 0){
        initExport();
    }

    function initExport(){
        form_export.on('submit', function (e) {
            e.preventDefault();

            let formData = new FormData(jQuery(this).get(0));

            jQuery.ajax({
                type: 'POST',
                url: API_CALL.export_consents,
                headers: { 'X-WP-Nonce': API_CALL.nonce},
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (json) {
                    if (json.data.consentment_file_base64 && json.data.consentment_filename) {
                        download(json.data.consentment_file_base64 , json.data.consentment_filename, "application/octet-stream");
                    }
                    if (json.data.message) {
                        alert(json.data.message);
                    }
                }
            });
        })
    }
});