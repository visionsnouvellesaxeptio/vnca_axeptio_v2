jQuery(document).ready(function () {
    let select_newsletter_system = jQuery('#select_newsletter_system');
    let select_newsletter_system_widget = jQuery('#select_newsletter_system_widget');
    var old_value = select_newsletter_system_widget.val();
    var form = select_newsletter_system_widget.closest('form');
    var button_form = jQuery(form).find('input[type=submit]');

    if(old_value === ''){
        deleteWebHookAxeptio();
    }

    if (API_CALL != undefined) {
        select_newsletter_system_widget.on('change', function (event) {
            if (old_value === '' && jQuery(this).val() !== '') {
                createWebHookAxeptio();
            }else if(jQuery(this).val() === '') {
                deleteWebHookAxeptio();
            }
            old_value = jQuery(this).val();
        });

        select_newsletter_system.on('change', function (event) {
            if (jQuery(this).val() !== ''){
                button_form.on('click', function (e) {
                    e.preventDefault();
                    let formData = new FormData(jQuery(form).get(0));

                    formData.delete('_wpnonce');
                    jQuery.ajax({
                        type: 'POST',
                        url: API_CALL.newsletter_validate_connexion_and_initialize,
                        headers: { 'X-WP-Nonce': API_CALL.nonce},
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        data: formData,
                        async:false,
                        success: function (data) {
                            button_form.off('click');
                            button_form.click();
                        },
                        error: function () {
                            e.preventDefault();
                            e.stopImmediatePropagation();
                        }
                    });
                })
            } else {
                button_form.off('click');
            }
        })
    }

    function createWebHookAxeptio() {
        jQuery.ajax({
            type: 'POST',
            url: API_CALL.create_webhook_axeptio,
            headers: { 'X-WP-Nonce': API_CALL.nonce},
            contentType: false,
            processData: false,
            dataType: "json",
            async:true,
            success: function (data) {
                // TODO: remove loader
                // console.log(data);
            },
            error: function (data) {
                // TODO: remove loader
                // console.log(data);
            },
        });
    }

    function deleteWebHookAxeptio() {
        jQuery.ajax({
            type: 'DELETE',
            url: API_CALL.delete_webhook_axeptio,
            headers: { 'X-WP-Nonce': API_CALL.nonce},
            contentType: false,
            processData: false,
            dataType: "json",
            async:true,
            success: function (data) {
                // TODO: remove loader
                // console.log(data);
            }
        });
    }
});