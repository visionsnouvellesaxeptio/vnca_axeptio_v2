//fontion générale axeptio
(function () {
    "use strict";

    // definition des parametres de base du plugin
    var options = {
        id: null,
        id_project: null,
        id_consentement: null,
        id_widget_container: null,
        axeptio_token: null,
        check_consent: null,
        email_id: null,
        routes: null,
        required: 1
    };

    var initialized = false;

    /**
     * @param change_options
     */
    jQuery.fn.wp_axeptio_v2_form_RGPD = function (change_options) {
        var option = jQuery.extend(true, {}, options);

        //change default option by change_options
        option = (change_options) ? jQuery.extend(option, change_options) : option;

        var $form = jQuery(this);

        //init
        option.email = getUsedEmailJqueryElmentToConsent(option, $form);

        checkValidityAndRemoveEventForm($form);

        if (option.email !== undefined && option.email !== null && option.email !== '') {
            if (jQuery(option.email).attr('id') == undefined) {
                let token = generateToken(5);
                jQuery(option.email).attr('id', token);
                option.email_id = '#' + token;
            } else {
                option.email_id = '#' + jQuery(option.email).attr('id');
            }
            loadBoxAxeptio(option);
        }
        else {
            console.log('Email not found. Please set email input with attribute type=email or id="email.');
        }

        function loadBoxAxeptio(option) {
            void 0 === window._axcb && (window._axcb = []);
            window._axcb.push(function (axeptio) {
                var jquery_email = jQuery(option.email);
                var email_change;

                if (jquery_email != undefined) {
                    jquery_email.unbind('keydown');
                    jquery_email.keydown(function () {
                        clearTimeout(email_change);
                        email_change = setTimeout(function () {
                            if (validateEmail(jquery_email.val())) {
                                mountwidget();
                                jQuery.ajax({
                                    method: 'GET',
                                    url: option.routes.get_consent,
                                    dataType: "json",
                                    async: false,
                                    data: {
                                        email: jquery_email.val()
                                    },
                                    success: function (res) {
                                        if (res.data.token && res.data.token != '') {
                                            axeptio.token = res.data.token; // récupération du token pour ceux qui sont déjà passé sur le site // 1 mail a 1 token
                                            axeptio.setToken(axeptio.token);
                                        } else {
                                            var token = generateToken(22);
                                            axeptio.token = token;
                                        }
                                    }
                                });

                                // On change le token dans le widget
                                var newToken = axeptio.token;
                                jQuery('input[name="' + option.axeptio_token + '"]').val(newToken); //exemple de token récupéré lié à l'email saisi
                                axeptio.setToken(newToken);
                            }
                        }, 1000);
                    });
                }


                //on monte le widget
                function mountwidget() {
                    let container = jQuery('#' + option.id_widget_container);
                    let vnca_container = container.closest('.vnca_container_axeptio');
                    axeptio.mountWidget({
                        service: "processings",
                        name: option.id_consentement,
                        node: container.get(0), //endroit où le widget doit s'afficher // créer un node par wiget, ne pas en mettre 2 dans un 1,
                    });

                    container.addClass('container-box-axeptio');

                    // container.data('required', option.required);
                    // vnca_container.data('required', option.required);
                    container.attr('data-required', option.required);
                    vnca_container.attr('data-required', option.required);

                    container.attr('data-email', option.email_id);
                    vnca_container.attr('data-email', option.email_id);

                    container.attr('data-box', option.id_consentement);
                    vnca_container.attr('data-box', option.id_consentement);
                }


                // Cet événement est appelé dès lors qu'un consentement
                // est enregistré dans l'API (attention, c'est un soit
                // un consentement positif, soit un refus.
                if (!initialized) {
                    axeptio.on('consent:saved', function (payload) {
                        isUserConsentResponseValid(payload)
                    });
                    axeptio.on('consent:fetched', function (payload) {
                        isUserConsentResponseValid(payload)
                    });
                    initialized = true;
                }

                mountwidget();
            });
        }

        // function of validation and parameters
        function checkValidityAndRemoveEventForm(form) {
            form.submit(function (element) {
                element.preventDefault();
                jQuery(this).addClass("was-validated");
                // jQuery(this).trigger('submit');

            });

            if (is_wpf7_form()) {
                form.submit(function (element) {
                    jQuery(this).addClass("was-validated");
                    if (jQuery(this)[0].checkValidity() === true) {
                        wpcf7.submit(jQuery(this));
                    } else {
                        element.preventDefault();
                        element.stopPropagation();
                        element.stopImmediatePropagation();
                    }
                });
            } else {
                form.on("submit", function (element) {
                    if (jQuery(this)[0].checkValidity() == false) {
                        element.preventDefault();
                        element.stopPropagation();
                    }
                    jQuery(this).addClass("was-validated");
                });
            }
        }

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }

        function getUsedEmailJqueryElmentToConsent(option, form) {
            let email = null;

            function emailExist(email) {
                if (email === undefined || email.length === 0)
                    email = null;
                return email;
            }

            // the email is defined by the user
            if (option.email_id !== undefined &&
                option.email_id !== null &&
                option.email_id !== ''
            ) {
                email = form.find('#' + option.email_id);
                email = emailExist(email);
            }

            // the input with id mail is defined
            if (email === null) {
                email = form.find('#email');
                email = emailExist(email);
            }

            // the input with type=email is defined
            if (email === null) {
                email = form.find('input[type=email]').get(0);
                email = emailExist(email);
            }

            return email;
        }

        function existAndSet(value) {
            return jQuery.inArray(value, [null, undefined, ""]) === -1
        }

        function generateToken(length) {
            let stringLength = length;

            // list containing characters for the random string
            var stringArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

            let rndString = "";

            // build a string with random characters
            for (let i = 1; i < stringLength; i++) {
                let rndNum = Math.ceil(Math.random() * stringArray.length) - 1;
                rndString = rndString + stringArray[rndNum];
            }

            return rndString;
        }

        //attention la validité du consentement dépend si c'est un champ requis ou non (required)
        // Exemple : dans le shortcode required=false et payload.consent.accept = false =>  réponse valide
        function isUserConsentResponseValid(payload) {
            //console.log(payload);
            //recuperation du nom du widget
            let box_name = payload.widget.props.name;
            // let input_box = jQuery('input[data-for=' + box_name);

            //pour chaque widget axeptio
            jQuery('.vnca_container_axeptio[data-box="'+ box_name +'"]').each(function () {
                let container = jQuery(this);
                let email = container.data('email');
                let require = container.data('required'); //permet de savoir si la réponse doit être positive (j'accepte)
                let form = container.closest('form');
                let vn_input_validation = container.find('.vn-box-validation') //input qui sert à valider la réponse (positive ou négative en fonction du require)
                email = form.find(email);

                if (validateEmail(email.val())) {
                    if (require == 1 && payload.consent.accept == true || require == 0) {
                        vn_input_validation.attr('checked', true);
                        vn_input_validation.prop( "checked", true );
                    } else {
                        vn_input_validation.attr('checked', false);
                        vn_input_validation.prop( "checked", false );
                    }

                    // demande d'enregistrement du token lié au mail
                    jQuery.ajax({
                        method: 'POST',
                        url: option.routes.save_consent, //option vient de localize
                        dataType: "json",
                        async: false,
                        data: {
                            email: email.val(),
                            token: payload.consent.token
                        },
                        success: function (data) {
                        }
                    });
                }
            });
        }

        function wpcf7_function() {
        }

        function is_wpf7_form() {
            try {
                existAndSet(wpcf7);
                return true;
            } catch (e) {
                return false;
            }
        }
    };


})(jQuery);