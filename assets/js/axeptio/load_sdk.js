//fontion générale axeptio
(function () {
    "use strict";

    var sdk_is_isset = false;

    function loadScriptAxeptio() {
        if (!sdk_is_isset) {
            let el = document.createElement('script');
            el.setAttribute('id', 'axeptio-sdk');
            el.setAttribute('src', 'https://static.axept.io/sdk.js');
            el.setAttribute('type', 'text/javascript');
            el.setAttribute('async', true);
            el.setAttribute('data-id', option.id_project); //id projet
            el.setAttribute('data-cookies-version', option.cookie_version); //version des cookies
            document.body.appendChild(el);
        }
        sdk_is_isset = true;
    }

    loadScriptAxeptio();

})(jQuery);