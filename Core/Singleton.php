<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 11/02/2020
 * Time: 11:31
 */

namespace vnca_axeptio\Core;

trait Singleton
{
    /**
     * Store the singleton object.
     */
    private static $singleton = false;

    /**
     * Singleton constructor.
     * @param $instance
     */
    private function __construct($instance) {
        if ($instance !== null) {
            $this->__instance($instance);
        }
        $this->__instance();
    }

    /**
     * Fetch an instance of the class.
     *
     * @param string|Object $instance
     * @return self
     */
    public static function getInstance($instance = null) {
        if (self::$singleton === false) {
            self::$singleton = new self($instance);
        }

        return self::$singleton;
    }

    /**
     * Set the instance of the
     * @param $instance
     * @return static
     */
    public static function setInstance($instance)
    {
        self::$singleton = self::getInstance($instance);
        return self::$singleton;
    }

    public function __instance(){

    }
}