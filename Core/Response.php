<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 18/02/2020
 * Time: 13:59
 */

namespace vnca_axeptio\Core;

class Response
{

    private $response = null;

    public function __construct($response)
    {
        $this->response = $response;
    }

    public function getBody()
    {
        return json_decode(\wp_remote_retrieve_body($this->response));
    }

    public function getCode()
    {
        return json_decode(\wp_remote_retrieve_response_code($this->response));
    }


}