<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 18/10/2019
 * Time: 13:30
 */
namespace vnca_axeptio\Core;

class Renderer
{
    public static function render($view, $attributes = []){
        ob_start();
        extract($attributes);
        require(VNCA_DIR_VIEW . $view);
        return ob_get_clean();
    }
}