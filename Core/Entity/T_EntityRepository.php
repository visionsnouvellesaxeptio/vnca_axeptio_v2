<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 21/11/2019
 * Time: 13:13
 */

namespace vnca_axeptio\Core\Entity;

trait T_EntityRepository
{
    function getArrayAttribute($array, $key, $return_false = '')
    {
        return (array_key_exists($key, $array)) ? $array[$key] : $return_false;
    }

    static function hydrate($parameters)
    {
        $path_object = get_called_class();

        if (is_object($parameters)) {
            $parameters = get_object_vars($parameters);
        }
        return new $path_object($parameters);
    }

    static function getRepository()
    {
        $path = self::$repository;
        return $path::getInstance();
    }

    static function hydrateAll($parameters)
    {
        $array_objects = [];
        foreach ($parameters as $object) {
            array_push($array_objects, self::hydrate($object));
        }

        return $array_objects;
    }

    /**
     * @return bool
     */
    public function flush()
    {
        return self::getRepository()->add($this);
    }
}