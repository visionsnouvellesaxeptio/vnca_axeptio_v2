<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 18/11/2019
 * Time: 17:59
 */

namespace vnca_axeptio\Core\Newsletter;



use vnca_axeptio\Core\Newsletter;

class Fact_Newsletter
{
    /**
     * @var I_Newsletter
     */
    private static $singleton = null;

    /**
     * Fact_Newsletter constructor.
     * @param $singleton
     */
    private function __construct($singleton) {
        if ($singleton === '' || $singleton === null) {
            $singleton = self::getNewsletterByString($this->getCurrentNameNewsletter());
        } else {
            $singleton = self::getNewsletterByString($singleton);
        }
        self::$singleton = $singleton;
    }

    /**
     * Fetch an instance of the class.
     *
     * @param string $instance
     * @return I_Newsletter
     */
    public static function getInstance($instance = null) {
        if (self::$singleton === null) {
            new self($instance);
        }

        return self::$singleton;
    }

    /**
     * Set the instance of the
     * @param string $instance
     * @return I_Newsletter
     */
    public static function setInstance($instance)
    {
        self::$singleton = self::getInstance($instance);
        return self::$singleton;
    }

    /**
     * @param $newsletter_string
     * @return I_Newsletter
     */
    public static function getNewsletterByString($newsletter_string)
    {
        $path = 'vnca_axeptio\\App\\Component\\Newsletter' . '\\' . $newsletter_string  . '\\' .$newsletter_string;

        if (class_exists($path)){
            $instance = $path::getInstance();
        } else {
            $instance = null;
        }

        return $instance;
    }

    static public function getCurrentNameNewsletter()
    {
        return  get_option('newsletter_system');
    }
}