<?php
/**
 * Created by <webmaster@visionsnouvelles.com>.
 * User: jonathan.poirot
 * Date: 18/11/2019
 * Time: 17:22
 */

namespace vnca_axeptio\Core\Newsletter;

use vnca_axeptio\App\Entity\ConsentementEntity;

interface I_Newsletter
{

    static function getInstance();

    /**
     * @param array $request
     * @return bool
     */
    public function isValideConnexion($request = []);

    /**
     * @return bool
     */
    public function initialized();

    /**
     * @return bool
     */
    public function uninitialized();



    /**
     * @param $request
     * @return bool
     */
    public function userUnsubscribe($request);

    /**
     * @param ConsentementEntity $user_data
     * @return bool
     */
    public function createUser(ConsentementEntity $user_data);

    /**
     * @param ConsentementEntity $user_data
     * @return bool
     */
    public function deleteUser(ConsentementEntity $user_data);


    public function createList();





    public static function getName();


}