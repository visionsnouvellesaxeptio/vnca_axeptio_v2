<?php
/**
 * @var $name_option string
 * @var $scripts [ 'id' => [ 'url', 'label', 'description' ] ]
 */

?>


<?php

use vnca_axeptio\App\Component\Shortcode;

$name_option = (isset($name_option) && $name_option != '') ? $name_option : null;
?>

<?php if ($name_option != null) { ?>
    <div class="wrap">
        <h1><?php _e('RGPD settings') ?></h1>
        <div class="container_setting_rgpd row flex justify-content-bettween flex-wrap">
            <form method="post" action="options.php" class="col-12 col-lg-8">
                <?php settings_fields($name_option); ?>
                <?php do_settings_sections($name_option); ?>

                <h2><?php _e('You need scripts ?') ?></h2>
                <!-- Information about axeptio  -->
                <table class="form-table">
                    <!--  Select id Axeptio  -->

                    <?php foreach ($scripts as $script_name => $option) { ?>

                        <?php $option['label'] = (!empty($option['label'])) ? $option['label'] : ''; ?>
                        <?php $option['description'] = (!empty($option['description'])) ? $option['description'] : ''; ?>

                        <tr valign="top">
                            <th scope="row">
                                <label for="vnca_axeptio_id"><?php echo apply_filters('the_title', $option['label']) ?></label>
                            </th>
                            <td>
                                <label class="switch">
                                    <input type="checkbox" name="<?php echo $script_name; ?>"
                                           id="<?php echo $script_name; ?>"
                                           value="1" <?php echo !empty(get_option($script_name)) ? 'checked' : ''; ?>>
                                    <span class="slider round"></span>
                                </label>

                                <?php if (!empty($option['description'])) { ?>
                                    <p class="description"><?php echo apply_filters('the_content', $option['description']) ?></p>
                                <?php } ?>

                                <?php if ((!empty($option['use_cdn']))) { ?>
                                    <label for="<?php echo $script_name; ?>_use_cdn"><?php _e('Use the CDN'); ?></label>
                                    <label class="switch switch-small">
                                        <input type="checkbox" name="<?php echo $script_name; ?>_use_cdn"
                                               id="<?php echo $script_name; ?>_use_cdn"
                                               value="1" <?php echo !empty(get_option($script_name . '_use_cdn')) ? 'checked' : ''; ?>>
                                        <span class="slider round"></span>
                                    </label>
                                <?php } ?>

                                <?php if ((!empty($option['use_custom_cdn']))) { ?>
                                    <div>
                                        <label for="<?php echo $script_name; ?>_use_cdn"><?php _e('Use custom CDN'); ?></label>
                                        <input type="text" name="<?php echo $script_name; ?>_use_custom_cdn"
                                               id="<?php echo $script_name; ?>_use_custom_cdn"
                                               value="<?php echo get_option($script_name . '_use_custom_cdn', ''); ?>"
                                               placeholder="<?php _e('Overload CDN are running :'); ?>">
                                    </div>
                                <?php } ?>

                            </td>
                        </tr>
                    <?php } ?>

                </table>

                <?php submit_button(); ?>
            </form>
        </div>
    </div>
<?php } else { ?>
    <div class="wrap">
        <span class="h1"><?php _e('Bad parameters', VNCA_TEXTDOMAIN); ?></span>
    </div>
<?php } ?>