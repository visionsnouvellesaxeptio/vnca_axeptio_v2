<?php

use vnca_axeptio\App\Component\Shortcode;
use vnca_axeptio\App\PageSetting\GeneralOption;

$name_option = (isset($name_option) && $name_option != '') ? $name_option : null;
?>

<?php if ($name_option != null) { ?>
    <div class="wrap">
        <h1><?php _e('GDPR settings', VNCA_TEXTDOMAIN) ?></h1>
        <div class="container_setting_rgpd row flex justify-content-bettween flex-wrap">
            <form method="post" action="options.php" class="col-12 col-lg-8">
                <?php settings_fields($name_option); ?>
                <?php do_settings_sections($name_option); ?>

                <h2><?php _e('Global settings', VNCA_TEXTDOMAIN) ?></h2>

                <!-- Information about axeptio  -->
                <table class="form-table">
                    <!--  Select id Axeptio  -->
                    <tr valign="top">
                        <th scope="row">
                            <label for="vnca_axeptio_id"><?php _e('ID Axeptio', VNCA_TEXTDOMAIN); ?></label>
                        </th>
                        <td>
                            <input type="text" name="vnca_axeptio_id" class="w-100"
                                   value="<?php echo esc_attr(get_option('vnca_axeptio_id')); ?>" required/>
                        </td>
                    </tr>

                    <!--  Select email axeptio  -->
                    <tr valign="top">
                        <th scope="row">
                            <label for="vnca_axeptio_email"><?php _e('E-mail Axeptio', VNCA_TEXTDOMAIN); ?></label>
                        </th>
                        <td>
                            <input type="text" name="vnca_axeptio_email" class="w-100"
                                   value="<?php echo esc_attr(get_option('vnca_axeptio_email')); ?>" required/>
                        </td>
                    </tr>

                    <!--  Select password axeptio  -->
                    <tr valign="top">
                        <?php $password = esc_attr(GeneralOption::getInstance()->getAxeptioPassword()); ?>
                        <th scope="row">
                            <label for="vnca_axeptio_password"><?php _e('Password Axeptio', VNCA_TEXTDOMAIN); ?></label>
                        </th>
                        <td>
                            <input type="password" name="vnca_axeptio_password" class="w-100" value="<?php echo $password; ?>" required/>
                        </td>
                    </tr>

                    <!--  SHORTCODE  -->
                    <tr valign="top">
                        <th scope="row">
                            <label for="shortCode"><?php _e('ShortCode', VNCA_TEXTDOMAIN); ?></label>
                        </th>
                        <td>
                            <input type="text" name="shortCode" class="w-100"
                                   value="<?php echo '[' . Shortcode\BoxAxeptioShortcode::$name_shortcode . ' id=' . __('NAME_OF_WIDGET', VNCA_TEXTDOMAIN) . ']'; ?>"
                                   readonly/>
                        </td>
                    </tr>

                    <!--  Select password axeptio  -->
                    <tr valign="top">
                        <th scope="row">
                            <label for="vnca_axeptio_text"><?php _e('Message Axeptio', VNCA_TEXTDOMAIN); ?></label>
                        </th>
                        <td>
                            <?php wp_editor(get_option('vnca_axeptio_text'), 'vnca_axeptio_text', array('textarea_rows' => 10, 'media_buttons' => false, 'class' => 'wp-editor-area')); ?>
                        </td>
                    </tr>

                    <tr valign="top">
                        <th scope="row">
                            <label for="vnca_axeptio_text"><?php _e('Load SDK to display cookies in all pages?', VNCA_TEXTDOMAIN); ?></label>
                        </th>
                        <td>
                            <?php $load_sdk = GeneralOption::getInstance()->getSdkInAllPages(); ?>
                            <input type="checkbox" name="vnca_axeptio_sdk_in_all_pages" id="vnca_axeptio_sdk_in_all_pages" value="1" <?php echo ($load_sdk) ? 'checked' : ''; ?>><?php _e('Yes, load the SDK to display cookies in all pages?', VNCA_TEXTDOMAIN); ?>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">
                            <label for="vnca_axeptio_text"><?php _e('Cookies version', VNCA_TEXTDOMAIN); ?></label>
                        </th>
                        <td>
                            <input type="text" name="vnca_cookie_version" class="w-100"
                                   value="<?php echo esc_attr(get_option('vnca_cookie_version')); ?>" />
                        </td>
                    </tr>
                </table>

                <?php if (!empty($processings)) { ?>
                    <h3><?php _e('Widgets available on your Axeptio account that can be used on your site', VNCA_TEXTDOMAIN); ?></h3>
                    <table class="list_widget_name form-table">
                        <?php foreach ($processings as $id => $name) { ?>
                            <tr valign="top">
                                <th scope="row">
                                    <label><?php echo $name; ?></label>
                                </th>
                                <td>
                                    <input type="text" name="<?php echo $id; ?>" class="w-100" value="<?php echo '[' . Shortcode\BoxAxeptioShortcode::$name_shortcode . ' id=' . $name . ']'; ?>" readonly/>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>


                    <br>
                    <h2><?php _e('Newsletter') ?></h2>

                    <!--  Select Newsletter  -->
                    <table class="form-table">
                        <!--  Widget use to the newsletter  -->
                        <tr valign="top">
                            <th scope="row">
                                <label for="newsletter_system"><?php _e('Widget', VNCA_TEXTDOMAIN); ?></label>
                            </th>
                            <td>
                                <?php $option = get_option('newsletter_system_widget'); ?>
                                <select name="newsletter_system_widget" id="select_newsletter_system_widget">
                                    <option value="" <?php echo ($option == '') ? 'selected' : '' ?>><?php _e('Select newsletter widget', VNCA_TEXTDOMAIN) ?></option>

                                    <?php foreach ($processings as $id => $name) { ?>
                                        <option value="<?php echo $id ?>" <?php echo ($option == $id) ? 'selected' : '' ?>>
                                            <?php echo $name ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>

                        <!--  Select Newsletter  -->
                        <tr valign="top" id="select_newsletter_system_content">
                            <th scope="row">
                                <label for="newsletter_system"><?php _e('Newsletter system', VNCA_TEXTDOMAIN); ?></label>
                            </th>
                            <td>
                                <?php $option = get_option('newsletter_system'); ?>
                                <select name="newsletter_system" id="select_newsletter_system">
                                    <option value="" <?php echo ($option == '') ? 'selected' : '' ?>><?php _e('Select newsletter system', VNCA_TEXTDOMAIN) ?></option>
                                    <option value="SendinBlue" <?php echo ($option == 'SendinBlue') ? 'selected' : '' ?>>
                                        <?php _e('SendinBlue', VNCA_TEXTDOMAIN) ?>
                                    </option>
                                </select>
                            </td>
                        </tr>
                    </table>

                    <div class="content_newsletter_system">
                        <!--  SendinBlue  -->
                        <table class="form-table" id="SendinBlue">

                            <!--  Select Newsletter  -->
                            <tr valign="top">
                                <th scope="row">
                                    <label for="shortCode"><?php _e('Description', VNCA_TEXTDOMAIN); ?></label>
                                </th>
                                <td>
                                    <?php $name = \vnca_axeptio\App\Component\Newsletter\SendinBlue\SendinBlue::getName(); ?>
                                    <?php wp_editor(get_option('vnca_' . $name . '_description'), 'vnca_' . $name . '_description', array('textarea_rows' => 10, 'media_buttons' => false, 'class' => 'wp-editor-area')); ?>
                                </td>
                            </tr>

                            <!--  Select Newsletter  -->
                            <tr valign="top">
                                <th scope="row">
                                    <label for="shortCode"><?php _e('API KEY (V3)', VNCA_TEXTDOMAIN); ?></label>
                                </th>
                                <td>
                                    <input type="password" name="vnca_sendinblue_token" class="w-100"
                                           value="<?php echo esc_attr(get_option('vnca_sendinblue_token')); ?>"/>
                                    <p class="description"><?php _e('Click on the link to get your API key', VNCA_TEXTDOMAIN); ?>
                                        :
                                        <a href="https://account.sendinblue.com/advanced/api" target="_blank"
                                           rel="noopener">https://account.sendinblue.com/advanced/api</a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </div>

                <?php } ?>
                <?php submit_button(); ?>
            </form>

            <div class="col-12 col-lg-4 justify-content-center">
                <?php if (!empty($processings)) { ?>

                    <form method="post" class="export-csv-container m-auto" id="export_consents">
                        <h2><?php _e('Export the consents', VNCA_TEXTDOMAIN) ?></h2>
                        <table class="form-table">
                            <tr valign="top">
                                <td>
                                    <select name="widget_name" class="w-100">
                                        <option value=""><?php _e('Widget name', VNCA_TEXTDOMAIN) ?></option>

                                        <?php foreach ($processings as $id => $name) { ?>
                                            <option value="<?php echo $id ?>">
                                                <?php echo $name ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td>
                                    <input type="text" name="email" class="w-100"
                                           placeholder="<?php _e('E-mail', VNCA_TEXTDOMAIN); ?>"/>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td>
                                    <input type="checkbox" name="valid_consent"/>
                                    <label><?php _e('Export only valid consent', VNCA_TEXTDOMAIN) ?></label>
                                </td>
                            </tr>
                        </table>
                        <button type="submit" class="w-100"><?php _e('Download', VNCA_TEXTDOMAIN); ?></button>
                    </form>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="wrap">
        <span class="h1"><?php _e('Bad parameters', VNCA_TEXTDOMAIN); ?></span>
    </div>
<?php } ?>