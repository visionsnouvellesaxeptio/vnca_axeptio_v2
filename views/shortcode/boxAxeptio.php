<?php
use vnca_axeptio\App\Api\Controller\Controller;

$id = (isset($id) && $id != '') ? $id : rand(0, 99);
$email_id = (isset($email_id) && $email_id != '') ? $email_id : '';
$check_consent = (isset($check_consent) && $check_consent != '') ? $check_consent : 'check_consent_' . $id;
$axeptio_token = (isset($axeptio_token) && $axeptio_token != '') ? $axeptio_token : 'axeptio_token_' . $id;
$widget_axeptio_container = (isset($widget_axeptio_container) && $widget_axeptio_container != '') ? $widget_axeptio_container : 'widget_axeptio_container_' . $id;
$id_project = (isset($id_project) && $id_project != '') ? $id_project : get_option('vnca_axeptio_id');
$id_consentement = (isset($id_consentement) && $id_consentement != '') ? $id_consentement : 'RGPD_contact';
$routes = (isset($routes) && is_array($routes) && !empty($routes)) ? $routes : ['get_consent' => Controller::getRoute('get_consent'), 'save_consent' => Controller::getRoute('save_consent')];
$required = (isset($required)  && $required == 'false') ? 0 : 1;
?>

<div class="vnca_container_axeptio">
    <div class="form-group">
        <div class="<?php echo $widget_axeptio_container; ?>" id="<?php echo $widget_axeptio_container; ?>">
            <!-- The axeptio box will load here -->
        </div>
        <input type="hidden" name="<?php echo $axeptio_token; ?>" value="">
    </div>
    <div class="form-group">
        <input type="checkbox" class="d-none form-control vn-box-validation" id="<?php echo $check_consent; ?>" data-for="<?php echo $id_consentement; ?>" name="<?php echo $check_consent; ?>" required/>
        <div class="invalid-feedback">  <?php _e('Please give your consent.', VNCA_TEXTDOMAIN); ?></div>
    </div>
</div>

<script>
    var VNCA_axeptio_call = function () {
        let id = <?php echo $id; ?>;
        let id_project = '<?php echo $id_project; ?>';
        let id_consentement = '<?php echo $id_consentement; ?>';
        let id_widget_container = '<?php echo $widget_axeptio_container; ?>';
        let axeptio_token = '<?php echo $axeptio_token; ?>';
        let check_consent = '<?php echo $check_consent; ?>';
        let email_id = '<?php echo $email_id; ?>';
        let routes = <?php echo json_encode($routes, JSON_PRETTY_PRINT); ?>;
        let required = '<?php echo $required; ?>';

        jQuery(document).ready(function () {
            var $check_consent = jQuery("#" + check_consent);
            var form = $check_consent.closest("form");

            if (form.length > 0) {
                jQuery(form).wp_axeptio_v2_form_RGPD({
                    id,
                    id_project,
                    id_consentement,
                    id_widget_container,
                    axeptio_token,
                    check_consent,
                    email_id,
                    routes,
                    required
                });
            }
        });
    };

    VNCA_axeptio_call();

</script>