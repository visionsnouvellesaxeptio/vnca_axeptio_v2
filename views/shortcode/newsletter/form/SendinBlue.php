<?php
/* @var $id string */
/* @var $description string */
/* @var $axeptio [id, name] */

?>

<form>
    <input type="email" placeholder="<?php echo _e('Email address'); ?>*">

    <?php if ($description) { ?>
        <div class="description">
            <?php echo apply_filters('the_content', $description); ?>
        </div>
    <?php } ?>

    <?php echo do_shortcode("[vnca_box_axeptio id=". $axeptio['name'] ."]"); ?>
</form>
